---
layout: markdown_page
title: Product Direction - Growth
canonical_path: "/direction/growth/"
---

## Growth Section Overview

The Growth section at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open core product. Our goal is to accelerate and maximize GitLab user and revenue growth, and while doing that, to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our Growth section consists of 4 product groups (adoption, activation, conversion, expansion) and 1 [product analysis](https://about.gitlab.com/direction/product-analysis/) group. Each product group consists of a cross-functional team of Growth Product Managers, Developers, and UX/Designers, with shared analytics, QA and user research functions, and the product analysis group is focused on empowering the product and growth team with analysis and insights.

### Growth Section's Principles

In essence, we are a cross-functional team of product managers, engineers, designers and data analysts with a unique focus and approach to help GitLab grow. Since GitLab Growth Section is still relatively new, we'd like to share the principles we strive to operate under, which will act as our team vision, and also help the rest of the company understand the best ways to collaborate with us.   

#### Principle 1: The Growth section focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the Growth section focuses on connecting users with that value.  We do so by:
* Driving feature adoption by removing barriers and providing guidance
* Lowering Customer Acquisition Cost (CAC) by maximizing the conversion rate across the user journey
* Increasing Life time value (LTV) by increasing retention & expansion
* Lowering sales/support cost by using product automation to do the work

#### Principle 2: The Growth section sits in the intersection of Sales, Marketing, Customer success & Product

Growth teams are by nature cross-functional as they interact and collaborate closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to complement and maximize these team's work, and ultimately allow customers to get value from our product.

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While the Marketing Team works on creating awareness and generating demand via channels such as webinars, content marketing & paid ads, the Growth team can help lower friction in signup and trial flows to maximize the conversion rate, thus generating higher ROI on marketing spend
* While the Sales Team works on converting larger customers in a high touch manner,the Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make the sales process more efficient
* While the Core product Team works on building features to solve customer problems and pain points, the Growth team can help drive product engagement, create usage habit and promote multi-feature adoption
* While the Customer Success & Support Teams work on solving customers problems and driving product adoption via human interaction, the Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

#### Principle 3: The Growth Section uses a data-driven and systematic approach to drive growth

Growth teams use a highly data & experiment driven approach
* We start from clearly defining what success is and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. Growth model helps us identify key variables that contribute to GitLab's north star metric, and describes the connections between them at a high level.
  We continuously iterate our thinking about GitLab's growth model as we learn more about our business, customers, and product, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing).


```mermaid

graph LR
	A[ARR] --> B[ARR from New Paying Customers]
	A --> C[ARR from Existing Paying Customers]
	B --> D[New customers directly sign up for Paid Plan]
	D --> E[# of Visitors]
	D --> F[Conversion Rate]
	D --> G[Average Contract Value]
	B --> H[New Trial Customers sign up for Paid Plan ARR]
	H --> I[# of Trials]
	H --> J[Conversion Rate]
	H --> K[Average Contract Value]
	B --> L[New/Existing Free Users sign up for Paid Plan ARR]
	L --> M[# of Free User Base]
	L --> N[Conversion Rate]
	L --> O[Average Contract Value]
	C --> P[Existing Paid Customer ARR]
	C --> R[Existing Customer Churned ARR]
	C --> Q[Existing Customer Expansion ARR]
	P --> S[Renewal]
	Q --> T[Seats]
	Q --> U[Upgrades]
	T --> V[# of Seats]
	T --> W[$ per Seat]
	U --> X[# of Upgrades]
	U --> Y[Delta ARR $]
	R --> Z[Cancel]
	R --> AA[Downgrade]
	AA --> AB[# of Downgrades]
	AA --> AC[Delta ARR $]
	subgraph Retention
	S
	Z
	end
	subgraph Expansion
	T
	U
	V
	W
	X
	Y
	AA
	AB
	AC
	end
	subgraph Acquisition
	E
	F
	G
	end
	subgraph Conversion
	I
	J
	K
	M
	N
	O
	end

```

Note that the terms used in growth model are broadly defined, for example, "Conversion Rate" here refers to the percentage of various non-paid customers (new, trial, free) converting to paid customers.

* Then by reviewing the sub-metrics, we try to identify the areas with highest potential for improvement, and pick focus area KPIs for those areas. We typically do so by looking for signals such as a very low conversion rate in an important funnel, metrics that seem to be low comparing with industry benchmarks, or a product flow that caused a lot of customer complaints or support tickets etc.         
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI. We utilize an experiment template to capture the hypothesis all the way to the experiment design, rank all experiment ideas using ICE framework, work through the experiment backlog, and then analyze the experiment results.
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next areas we want to focus on

```mermaid
graph LR
    nsm[North Star Metric]--> grm
    grm[Growth Model] --> fak
    fak[Focus Area KPI] -- Data --> ide
    ide[Ideate] --> pri
    pri[Prioritize] --> bld
    bld[Build] --> alz
    alz[Analyze] --> tst
    tst[Test] --> ide

    style nsm fill:#6b4fbb
    style nsm color:#ffffff
    style grm fill:#6b4fbb
    style grm color:#ffffff
    style fak fill:#fba338
    style fak color:#ffffff
    style ide fill:#fa7035
    style ide color:#ffffff
    style pri fill:#fa7035
    style pri color:#ffffff
    style bld fill:#fa7035
    style bld color:#ffffff
    style alz fill:#fa7035
    style alz color:#ffffff
    style tst fill:#fa7035
    style tst color:#ffffff
```

By following this systematic process, we try to make sure that: 1) We know what matters most; 2) Teams can work independently but their efforts will all contribute to overall growth; 3) We always aim to work on the projects with the highest ROI at any given moment. The ultimate goal is that we are not only uncovering all levers to drive growth, but also tackling them as efficiently as possible.
#### Principle 4: The Growth Section experiments a lot and has a “Win or Learn”  mindset

***"Experiments do not fail, hypotheses are proven wrong"***

Growth teams view any changes as an “experiment”, and we try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful in term of driving desired metrics. Refer to section below for a list of experiments

### How we launch experiments at GitLab? 

Want to launch product experiments too? Check out this [slides deck](https://docs.google.com/presentation/d/1nmStWChWkYad9K-dced9wS4jS7XLIrHB-WKafc7jrMU/edit#slide=id.gca4c496ea4_0_0) the GitLab growth team put together

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rEHxAfxr9eU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->


## Growth Section's Current Focus Area

Starting in FY21 Q4, Growth section will take a more focused approach to work on the highest ROI growth opportunities for the business - and the first focus area we've chosen is the new customer free to paid SaaS conversion rate.

Through analysis of our new customer acquisition funnel, we identified there is reasonable room for improvement in this area. In the past year, Growth section has launched many experiments and improvements in this area, such as new user onboarding issue board experiment, improved check-out experience etc, and we have accumulated insights and learnings to allow us to form high quality hypothesis. One key insight we've learned is that first 90 days is critical to a new customer's onboarding, and if this customer successfully adopts our key features in Create & Verify, invites team members to join, and experiences the GitLab's product value via a trial, the customer's likelihood to convert increases significantly.     

Based on this insight, we will have each of the growth groups to focus on one key actions:
- Create Adoption: Create is the first stage most new customers need to set up, and many new customers are here for SCM. If we can improve the adoption rate of create early on, it sets up the customer for success for adopting more features and converting.
- Verify Adoption: Among all stages, using Verify actually has highest correlation with converting to paid customers, we believe this is the aha moment where new customers see the value of a complete devops platform.
- Invite team members: GitLab is designed for teams, if we can get eligible namespaces to effectively add more team members, they are more likely to see the value of GitLab, and we believe that can drive conversion rate.
- Trial to paid conversion: Trial is one of the most effective ways to get new customers to upgrade, and we think there is room for improvement in terms of trial to paid conversion rate. We want to improve this before driving more trials.

This way, we can launch experiments in these 4 areas that can potentially help a new prospect customer see the value of GitLab quickly, and increase their likelihood of conversion. Because there are opportunities to drive 4 actions in some shared new customer flows and touch points, the 4 growth groups will collaborate closely to make sure we have a shared vision for the new customer journey, and won't create a situation that one customer is pulled too many different directions. We will start with our .com product, and migrate applicable learnings and improvements to our self-managed product.


## Growth Section's Mid-term Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab's Growth Section aims to help the company drive growth while improving efficiency in all fronts. Therefore we've aligned our 3-Year Strategy into the the themes below:

## Maximize new customers conversion

##### Theme 1: Improve trial experience to drive new customer growth

- KPI: trial volumes, trial to paid conversion rate, free to paid conversion rate
- Key focus area: sign up experience, trial experience, onboarding


In order to grow efficiently,  we want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need to reduce payback time to generate cash flow that is essential for weathering storms.

As a collaborator to marketing & sales teams, the role the growth team can play here is to aggressively analyze, test and improve the new user flow and in-product experience between about.gitlab.com, GitLab.com, self-managed instance, CustomersDot etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signs up for a paid plan.

In order to achieve this goal, we try to understand what the drivers are leading to new customer conversion and amplify them. For example, we have identified through analysis that the number of users in a team, as well the numbers of stages/features a team tries out, all seem to be correlated with a higher conversion rate to a paid plan.

Again, in order to drive this theme, we will also need to understand the end to end new customer journey, and identify the drivers and barriers via a series of research and analytics projects, such as:


1) Map out GitLab new customers journey and understand any potential experience or data gaps between marketing, sales, growth, product teams

2) Post-purchase survey & Email survey to understand the reasons behind why some customers convert, while others don’t

3) Self-managed new customer user research


##### Theme 2: Build out product qualified leads(PQL) programs to become a scalable and effective leads source

- KPI: PQL volumes, PQL conversion rate
- Key focus area: free namespaces

###### Overview
The community of free users on GitLab represents an economic moat for our business. However, there is more untapped potential within this community of users. As a free user of GitLab, it can be challenging while using the product to understand if a paid feature exists that could improve their current task. We plan to address this problem via two avenues. First by investing in a unified feature discovery experience in the product where users can learn about paid features and tiers at contextually relevant moments where we will present them with options on how to proceed including things like "upgrade now" "start a trial" and "talk to sales". And second by investing in usage-PQLs where we will look at usage patterns within the product to make recommendations to users to assist in their adoption journey and for qualified users getting them connected with their sales representative.

###### Current state of PQLs today in the GitLab product
Currently, there are two hand-raise PQL moments in GitLab. The first was introduced at the Cape Town Contribute (called an in-app health check at the time). The way it works is if a free self-managed instance is over a particular user count and has usage ping turned on then we display an option in the top right dropdown to “Get a free instance review” (see screenshot below).
<img src="/images/growth/health_check_dropdown.png" width="250">

If a user selects this instance review option they are brought to the following form
<img src="/images/growth/health_check_form.png" width="600">

Between this being implemented during the Cape Town Contribute in 2018 and the first half of 2020 it generated a total of 28 leads, 3 became customers for a total of $93K in incremental annual contract value(IACV). In the fall of 2020, the Growth team increased the rollout of this “instance review” link from previously only appearing if a free instance had 100+ users to displaying if the instance has 5+ users. In the first two months of 2021, the instance review option has generated 26 leads.

There are two core reasons that the cape town PQL moment is underperforming as a PQL lead source.
1. The “Get a free instance review” is not contextually relevant to the instances’ actual usage of GitLab it simply appears if you had over 100 users (now when you have 5+). There’s no clear value to the user as to why they should select this CTA. It’s not clear how this will make them better at their job or why it will improve their experience within GitLab.
2. The form they have to submit is daunting and again doesn’t provide any context on who they will be speaking with or what value they will get from that conversation.

The second hand-raise PQL point is on the SaaS in-app billing page. In 2020 the Growth team added an MVC PQL point into the SaaS product, whereby if an admin navigates to the in-app billing page they now along with the option to upgrade have an option to “Contact sales”. If a user selects “Contact sales” they are brought to the [marketing website form](https://about.gitlab.com/sales/) for contact sales.

<img src="/images/growth/contact_sales_on_in-app_billing_page.png" width="250">

When the growth team first implemented the “Contact sales” option on the billings page it was run as an A/B test. By including the option to “contact sales” we increased the overall engagement with a primary CTA (upgrade now or contact sales) by x% compared to the control. This validated that there were some users interested in speaking to sales that previously did not take action on the page. To date, we’re consistently seeing a 1% click-through-rate on viewing the in-app billing page to clicking “Contact sales”.

Where the experience can be improved:
1. The redirect from a user being in-app to the marketing website is a jarring visual shift and the marketing form leaves a lot of exit points open i.e. the main website header.
2. When we redirect the user to the marketing site we lose access to the information we already know about them causing the form to be longer. If the form was in the product experience we could remove things like first name, last name, email address reducing the form length and increasing the submission rate.
3. We can provide more context in the in-app experience on the value of the particular tier and why it’s worthwhile to connect with a salesperson

###### The future vision for  hand-raise PQLs
Hand-raise PQLs, trials, and upgrades scale linearly with increased awareness and value in the paid tiers. The keyword here is awareness, currently, our free users are mostly unaware of the additional value they could get from the paid tiers as it’s on the user to navigate to our marketing site or support docs to see if we have a paid feature and what tier it’s in.

We recently conducted a survey of Bronze and Premium SaaS customers asking them what features they were aware of, what features they had used, what features they are interested in. By looking at the features users indicated they are very or somewhat interested in divided by the users that have indicated they’ve never used these features we can identify areas of the product that users would find valuable but they haven’t yet discovered them (raw data).

![image](/images/growth/survey_of_feature_awareness.png)

By seeing that paid users on these tiers are unaware of these features they’re interested in we know that we need to increase the visibility for free users so they can perceive the value of the paid tiers. We need to bring this contextually relevant information to the users at the right moment through a consistent product experience and provide them with the opportunity to self-select on how they’d like to proceed “Start a trial” “Upgrade now” “Talk to sales”.

The growth team has built one of these feature discovery moments (seen below) which in the past three months has generated 1,700 “upgrade now” clicks and 650 “start a trial” clicks when combined with close rates and ASPs the estimated ARR is $38K from this one feature discovery moment ([source](https://app.periscopedata.com/app/gitlab/591514/WIP:-Growth---Conversion---Experiment---Security-Upgrade-Point)).

![image](/images/growth/feature_discovery_moment.png)

The vision is to turn this one feature discovery moment into a universal modal that any product team can easily deploy into their area of the product. This allows us to increase awareness of our paid tiers, provide the needed context to the users at the right moments, and reduces the development time for individual teams to implement the experience. This is how we scale from feature discovery moment today to many dozens while still ensuring we’re providing the best possible user experience.

As we scale this feature discovery moment experience throughout the product it’s going to be imperative that the Growth team partner with the data, marketing, and sales teams to monitor all feature discovery moment views, what CTAs (‘start a trial’, ‘upgrade now’, ‘talk to sales’) were selected and their associated close rates and ASPs. This is how we can ensure that we understand what sources are driving the most value for the business while also watching out for underperforming sources that may be able to be removed which ensures we don’t clutter the UI. By looking at the volume of these CTA clicks multiplied by their trialing 90-day close rates and ASPs will allow the product team to report on in-month product-driven revenue.

Over time the growth team will focus on monitoring and improving volume while also working to increase the view to action rate for these feature discovery moment CTAs. How can we make it easier to start a trial from these experiences as close to one-click as possible? How can we reduce the required fields the user needs to fill out to connect with a salesperson? How can we reduce the time to connect with their salesperson, can we embed their assigned sales reps calendar directly within the product? Do we see lower work/close rates for hand-raise PQLs under a certain team size, should those teams only see self-service options? Should larger teams only see hand raise PQL options? By continuing to optimize this experience we can ensure we’re maximizing the value from our free user community while also ensuring they have the best possible product experience.

###### The future vision for usage based PQLs
The ultimate vision is to establish a smart customer interaction system that will trigger the right interaction based on user ‘s product usage behavior to move users to next stage of lifecycle.  We are still in the early stages of these efforts.

What is a usage PQL? When we detect that certain GitLab users take certain actions or show lack of certain actions while using the product, we can use different interactions to guide them to take an action to move forward in the user lifecycle.

To start we are defining three sub-types of usage PQLs, education, in-app recommendations, and sales alerts. We will balance the internal effort (cost) and the potential return to determine the type of information we provide and the method we trigger.

![image](/images/growth/cost_vs_return_matrix.png)

To assist in user adoption and education we will start with usage based product onboarding where if the namespace has not taken important setup actions we'll recommend those actions to them, for example, setup Create, setup Verify, run your first security scan.

As we discover important actions overtime that warrants further investment we will make recommendations to users and teams within the product. For example, if a single user or a small team is running SAST scanning we may recommend to them in-app to start a trial to test out more advanced scanning features.

For high-value actions taken by larger accounts, for example, a large company with many users is actively running SAST scanning we should alert their assigned sales representative about this usage and the more advanced features available in Ultimate.

The first usage-PQL email series has launched to support [free user onboarding](https://gitlab.com/groups/gitlab-org/growth/-/epics/62#note_483748226) whereby we're sending users content recommending the next feature and/or stage they should adopt. The product team has also started brainstorming different potential usage PQLS across the three types in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/300049). This area will be a continued focus for Growth working across departments with Marketing, Sales, and Customer Success as more teams implement usage-based outreach.


## Drive feature adoption and usage

#### Theme 3: Onboard new users/customers effectively to support their use case

- KPI: new namespace stage adoption rate, time to adoption, new namespace SpO
- Key focus area: registration flow, continious onboarding, dashboard 

**Registration Flow Vision** 

A user's first interaction with GitLab is often through the creation of their account. This is our chance to make a good first impression and get them down the right path for them to have longer-term success. We have users of various types that have different goals depending on their role, if they're a solo user vs. a team, if they are setting up the account for a team vs. joining an existng one, and where they are in their adoption of DevOps. We currently have a one-size fits all approach though, and it assumes the user wants to set up a team along with a new group and project. 

Our vision is to direct users down the right path for them based on a few pieces of data that we'll collect in the registration flow. Those key data points include: 

- Are they creating a new project or team vs. joining an existing one? 
- Are they setting up for a group or just themselves? 
- Their JTBD

The flows will end up branching down a few distinct paths: 

- For users that indicate they are here to set up a new team or project for their company, we'll guide them to creating a group, project, and inviting users once they're in the product. This is the default registration flow that all users go through today. 
- For users that indicate they are here to join an existing team, we'll skip group and project creation and get them to join their team or project as quickly as possible. This work is [scheduled for 14.5](https://gitlab.com/gitlab-org/gitlab/-/issues/340560). 
- For users that are solo but are setting up a new project, we'll guide them to create a group and project but allow them to skip it 
- For users that indicate they are here to import their repo from somewhere else, we'll [guide them right to the import flow](https://gitlab.com/gitlab-org/gitlab/-/issues/336284)

```mermaid
graph LR
    A[Sign up] -->|Email Verification| B{Setting up vs. joining an existing team/project}
    B -->|Setting Up| C{Company or Personal? JTBD? Role?}
    B -->|Joining Existing Team| D[JTBD? Role?]
    C -->|Company| E[Group and project creation, skip not allowed]
    C -->|Personal| F{Group and project creation, skip allowed}
    C -->|JTBD = import from somewhere| G[Direct to import flow]
    E --> H[Trial, can be skipped]
    H --> I[Project Overview Page] 
    F -->|Skipped| J[Homepage]
    F -->|Not Skipped| I
    G -->L[Create Group]
    L -->M[Import project]
    D -->N[Homepage]
    N -->O[Guide to help find team]
```

Over time we'll continue to experiment with the steps and options in the flow, with the goal being to get users through it as quickly as possible while balancing that with getting them started off correctly in order for them to be successful. Some additional data points we want to utilize to customize the flow and onboarding include: 

- Overall experience level with Git and DevOps. We may provide more in-product guidance and introductory content for users that have less experience. 
- Users that are coming from other specific tools. We may provide "switcher's guides" to help users more easily make the transition from alternative tools. 
- Their stated JTBD. We may provide more guidance and onboarding content specific to their stated goals to help users discover value more quickly. 

In addition to flow changes, we also want to [improve the overall look and feel](https://gitlab.com/gitlab-org/gitlab/-/issues/327103) of the flow and the [ways that we collect the data we need](https://gitlab.com/gitlab-org/gitlab/-/issues/325296) to keep it engaging and a delightful first impression for new users. 

<img src="https://gitlab.com/gitlab-org/gitlab/uploads/825b3f5ea5918d73a89fce3ec2f371e4/image.png">


**Onboarding vision**

Currently, we have the ["Learn GitLab" project](https://gitlab.com/matejlatin/learn-gitlab-new/-/boards) which is a dedicated onboarding project with issues assigned to the user that signs up, the main challenge with this experience is users have to understand our group > project architecture to navigate between the learn GitLab project and their own project as well as remembering to navigate to this specific project for their remaining onboarding tasks. We've actively running [an experiment](https://gitlab.com/groups/gitlab-org/-/epics/4817) that we hope we're hoping to wrap up in 14.2 where we provide users with Learn GitLab in the left nav and dynamically update their percent completed based on the actions the namespace has completed (screenshot below). Our next [experiment](https://gitlab.com/gitlab-org/gitlab/-/issues/337534) here will be to determine if it's better to send users to onboarding issues or directly to the action in-app, we believe the latter is a better long-term user experience but we want to validate that through a test.
 
 <img src="/images/growth/continuousonboarding1.png" width="600">
 
 Our [longer term vision](https://gitlab.com/gitlab-org/gitlab/-/issues/336101) is this becomes more of a "home page" for the project where we display onboarding tasks along with other elements we know increase adoption/conversion rate, for example, an invite colleague, setup integrations modules along with helpful users specific modules like recent comments, MRs and/or To-dos. Over time we hope to become more intelligent and recommend PQL actions like "talk to sales" for users/namespace that reach specific thresholds. Example potential future component within the page are shown below:
  
<img src="/images/growth/continuousonboarding2.png" width="600">
 
The part of this vision that has yet to be fully defined is when a user selects an action how can we best walk them through that task. In previous user testing, we've found our users are not generally interested in multi-step tooltip tutorials they have to walk through in order to complete a task, they generally gloss over the steps eventually just closing them out. The other issue with multi-step tooltip tutorials is they're not easily scalable to multiple stages and have a tendency to break as the user experience is updated over time. What we'd like to explore is develop a universal experience that all stages can apply to assist in their onboarding. An example of this in the product today is a fly-out that the Verify team is testing, in the future we could potentially with some sort of visual element so it's easily recognizable to open/close and eventually dismiss this type of fly-out in the product. If you have feedback on our vision in this area please don't hesitate to reach out as we'd love to collaborate, you can find us on on slack at #s_growth or by creating an issue and mentioning @gitlab-org/growth/product-managers.
 
<img src="/images/growth/continuousonboarding3.png" width="600">
 
**Personalized stage onboarding**

Because GitLab is a complete DevOps platform, prospect users and customers start to use the product for different reasons. Based on our marketing team's research, there are several [use cases](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/) which are key to our GTM strategy and often are where we land in an account or are significant expand motions.

1. [Usecase: Version Control and Collaboration](/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/) (all project intellectual property - code, design, and more)
1. [Usecase: Continuous Integration](/handbook/marketing/strategic-marketing/usecase-gtm/ci/) (how teams automate and streamline build and test to improve quality and velocity)
1. [Usecase: DevSecOps](/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/) (how teams shift security left and make it relevant throughout the delivery lifecycle)
1. [Usecase: DevOps Platform](/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/) (making adoption of DevOps practices easier and streamlined, eliminating waste)

The data analysis also shows that new namespaces adopts different GitLab stages at different speed, which highglights the opportunity to provide personalized onboarding experience to our new users/customers, with potential different focus on Create, Verify, Secure adoptions.

**Switcher experience**

The other factor that impacts GitLab's new user experience significantly is whether they come from an incumbent or start from a clean slate. From user interviews, we understand that if a prospect comes from an existing solution, they often need to overcome more hurdles and their "onboarding" experience is quite different from others.  They sometimes need to maintain 2 solutions at the same time, have to reconfigure many integrations, and encounter errors or issues that are not very well covered in typical "new user onboarding". This is a potential direction the growth team can explore.

**User onboarding vs. Team onboarding**

GitLab is most valuable when used as a team collaboration tool, therefore not only each individual new user needs to learn how to use GitLab and find value, for teams we need to help the entire group to get settled and get to the team "aha moments" too. This will involve identifying a team use case early on, make the team invitation flow smooth and intuitive, and showcase the features most valuable to teams. 

**Paid customer onboarding**

For paid customers who subscribe to premium or ultimate, we also need to make sure they are guided properly to use the relevant paid features and reach the ideal seat utilization rate early on.  Our Customer Success team has a strong focus in this area, but growth can supplement that effort by building onboarding and nudge into the product flows.  

**Driving early Stage Adoption (SpO)** 

We’ve built a comprehensive DevOps Platform, and users are more succesfull and more likely to convert to paying customers with each additional stage that they use. If we can increase organization stage usage (SpO) in the first 30 days, we’ll see a large positive impact to free to paid conversion and revenue.   


#### Theme 4: Drive more stage adoption post onboarding

- KPI: Existing namespace SpO, stage cross-adoption rate  
- Key focus area: continious onboarding, in-product contextual upsell moments  

Post initial onboarding,  we still want to guide customers to use multiple features and stages. One hypothesis is that if a customer uses multiple stages and features of GitLab, they are more likely to get value from a single DevOps platform, thus they are more likely to become a long term customer.

Therefore, one of the Growth Section's focus areas will be on helping GitLab customers adopt more stages, and we'll observe if that leads to better retention to confirm the causation. ALong with this, we will need to conduct both [quantitative data analysis](https://gitlab.com/gitlab-data/analytics/-/issues/4762) and [qualitative research](https://gitlab.com/gitlab-org/growth/product/-/issues/1572) to describe the baseline and understand the drivers and barriers behind stage adoption.  

```mermaid
graph LR
    Create --> Plan
    Verify --> Secure
    Create --> Manage
    Create --> Verify
    Verify --> Release
    Verify --> Package
		Verify --> Configure
    Release --> Monitor
    Release --> Protect
```

#### Theme 5: Improve customer retention to build a solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments, it is critical to serve our current customers well and minimize churn. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability, and can be right there when customers are ready to expand again.

To improve retention, the Growth Section has identified and will be working on the areas such as:


1) Continue to make the customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical customer moments.  
We have done projects such as:
* Clarifying the options between “auto-renewal” and “cancel”
* [Providing clear information on customer licenses and usage](https://gitlab.com/gitlab-org/gitlab/-/issues/118592)
* Automating renewal processes to minimize sales team burden

And we are also working on quarterly co-term billing for self-managed customers, continuous improvement of CustomersDot user experience, as well as supporting critical pricing and packaging related projects.

To make sure what we do will benefit customer, we have a weekly cross-functional sync with sales, biz ops, support, finance and closely monitor Customer Success Score (billing related) as our OKR.

2)  Deep dive into the customer behavior and identify leading indicators for churn. This will include projects such as:  
* Creating [Customer MVC health score](https://gitlab.com/gitlab-data/analytics/-/issues/3803) and analyze usage based retention curve with help from data analytics team
* Capturing and analyzing [reasons for cancellation](https://app.periscopedata.com/app/gitlab/572742/Auto-Renew-AB-Test-Result?widget=7523649&udv=0)
* Experimenting with different types of “early interventions” to reduce churn



## Drive growth with insights from data and research

##### Theme 6: Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include:

1) Provide knowledge & [documentation](https://docs.google.com/presentation/d/1J0dctOP-xERo2j0WMWcaBuNqTNaaPuJTxLfIaOhNRHk/edit#slide=id.g74d58e3ba0_0_5) of currently available data sources to understand customer behaviors . Also, establish a framework and best practices to enable consistent and compliant data collection

2) Build out a Product usage metrics framework to evaluate how customers engage with certain features. For example, we successfully finished SMAU data collection and dashbaording project for all GitLab product stages in Q1, and will move on to a similar [North Star Metric project](https://gitlab.com/gitlab-org/product-intelligence/-/issues/374) for all GitLab product features in Q2.

3) Build out Customer journey metrics framework to understand how customers flow the GitLab through funnel, including [end to end cross-functional reporting](https://gitlab.com/gitlab-data/analytics/-/issues/4336) spanning marketing, sales and growth.   


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and that success ultimately comes from constant learning & iteration & testing & sharing, as well as breaking the silos of functional teams to drive towards the same goal.



## Growth Insights Knowledge Base

### Dashboards
Below is a list of the dashboards we build and use daily. Many of these dashboards are used not only by us, but also by executives, broader product team, sales and customer success teams.   

|Area |Dashboard/Report | Description |Date|
| ------ | ------ |------ |------ |
|Overall Product| [Product Adoption Dashboard](https://app.periscopedata.com/app/gitlab/771580/Product-Adoption-Dashboard)  | Product KPIs |FY22 Q1 |
|Overall Growth| [New namespace conversion dashboard](https://app.periscopedata.com/app/gitlab/761347/Group-Namespace-Conversion-Metrics)  | Leading indicators of how new namespaces convert to paid customers |FY21 Q4 |
|Overall Product| [SMAU dashboard](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard)  | Stage Monthly Active User trend for both .com and self managed |FY21 |
|Overall Product| [Stage Retention and Adoption Dashboard](https://app.periscopedata.com/app/gitlab/621735/WIP:-SMAU-Retention-and-Adoption) | How popular are the stages among GitLab customers, and how well are they retaining customers |FY21 |
|Activation |[New Customer Acquisition Dashboard](https://app.periscopedata.com/app/gitlab/531526/Acquisition-Dashboard---Last-90-days) |Trend of acquisition of new customers |FY21 |
|Conversion |[New group namespace trial to paid conversion rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate) | How are new new groups that start trials converting to becoming paid customers |FY21 |
|Adoption |[Renewal Dashboard](https://app.periscopedata.com/app/gitlab/505939/Renewals-Dashboard) | Trend on renewal and cancellation of susbscriptions|FY21 |
|Adoption |[Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention) | Retention metrics and trend |FY21 |
|Adoption |[What's New Dashboard](https://app.periscopedata.com/app/gitlab/816678/What's-New) | Information on engagement with What's New posts | FY21 | 
|Expansion | [New groups with at least 2 members in their first seven days](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added) | At what rate do new groups have at least 2 members and how can we increase the team adoption rate |FY21 |
|Expansion | [SPAN Deep Dive Report](https://docs.google.com/document/d/1zt3uUcPJW7Y1dMgtIsi4Z1antbPFPy8fp7Fib4Ps_qw/edit?usp=sharing) |How to understand SPAN (Stage Per Average Namespace) and ideas to improve SPAN|FY21 |
|Overall Product| [EoA Monitoring Metrics - Bronze Impact on Group Namespaces](https://app.periscopedata.com/app/gitlab/810873/EoA-Monitoring-Metrics---Bronze-Impact-on-Group-Namespaces) | How did the removal of the Bronze plan impact free-to-paid conversion as well as group namespace stage adoption |FY22 Q1 |
| Overall Product | [Stages per Organization](https://app.periscopedata.com/app/gitlab/824044/WIP---Stages-per-Organization-(SpO)-Deep-Dive) | What are the most common stage adoption paths for new group namespaces? How does the adoption of certain stages correlate with long-term engagement, expansion, and conversion? | FY22 | 
| Expansion | [Invite Acceptance Analysis](https://app.periscopedata.com/app/gitlab/863103/Invite-Acceptance-Analysis) | Analysis of the time from namespace creation to creating invites, users accepting invites, and adopting stages.| FY22 |
| Adoption | [Cross-Stage Adoption Dashboard](https://app.periscopedata.com/app/gitlab/869174/WIP-Cross-Stage-Adoption-Dashboard) | Analyze how organizations engage with different combinations of stages. | FY22|
| Adoption | [Retention (non-subscription based) of Users and Namespaces](https://app.periscopedata.com/app/gitlab/883332/WIP-User-and-Namespace-Retention) | What does namespace and user retention look like based on event actions instead of logins? | FY22 | 
| Adoption | [SSO Deep Dive](https://app.periscopedata.com/app/gitlab/897036/Growth:Adoption-Deep-Dive-on-SSO-Logins) | How do users signing up with SSO differ with respect to engagement and purchase? | FY22 | 
| Adoption | [Trial Adoption by Day](https://app.periscopedata.com/app/gitlab/883438/Day-0-Trial-Namespaces) | What does the distribution of trial adoption look like over the first week as namespace? | FY22  | 
| Retention | [SpO Retention](https://app.periscopedata.com/app/gitlab/902044/SpO-Retention---WIP) | What usage patterns at the end of a paid plan inform the likelihood of plan retention? | FY 22 |
| Adoption | [Golden Journey Pathways](https://app.periscopedata.com/app/gitlab/897587/Golden-Journey-Paths) | How effectively are namespaces and instances funneling from one stage to another? | FY 22 Q3 |

### Insights

|Link |Type| Description |Date |
| ------ | ------ |------ |------ |
|[Why prospects come to GitLab](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit?usp=sharing)|Survey|It shows top reasons propsects come to GitLab and % of win/loss by reasons |FY21 Q1 |
|[Feature Discovery Survey](https://docs.google.com/spreadsheets/d/1fAs9_5_wzZpdVs3d7D1Iqunb-scF5s_7xD9hNWHacNA/edit?usp=sharing) | Survey |Among our paid customers, which features are they aware, interested in and used? |FY21 Q1 |
|[Increase Stages per Organization](https://docs.google.com/presentation/d/1eyClYu8x2nLOhcNTcKx4-Z_IdtO6S2s9KhgImglcNCE/edit?usp=sharing)  | Analysis |How many stages does an average GitLab customer adopt and how to drive adoption |FY21 Q1|
|[What drives free to paid adoption](https://docs.google.com/presentation/d/11Ks8ArlCGzGY0VrSo68RBkbKwWD7pUEc9I9Vu0bIaR4/edit?usp=sharing) | Analysis |What are the drivers that can lead to higher free to paid conversion rate |FY20 Q4|
|[SaaS products new user onboarding research](https://docs.google.com/presentation/d/13BCqrAjY-guNdiRFXNY2ubLLmQdXUpiyaERxdEdirFA/edit#slide=id.g9dbb0f4ccd_0_0)|Research|How best-in-class SaaS products are designing their registration flow and continious onboarding experience|FY21 Q1|
|[Why are prospects coming to GitLab (utilizing Command Plan info)](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit#gid=1745528755)| Analysis | What is the distribution of primary use cases across various sales segment and closed/won vs. closed/lost. | 2021-03-01 |
|[Which What's New items are users most intersted in?](https://app.periscopedata.com/app/gitlab/816678/What's-New)| Dashboard | Dashboard that shows engagement with What's New items | ongoing |
|[Why are Saas users cancelling?](https://app.periscopedata.com/app/gitlab/808007/WIP-Cancellation-Reasoning-Dashboard)| Dashboard | Dashboard that shows the self-reported reasons and verbatims that Saas customers are cancelling in CustomersDot | ongoing |  
|[Free to paid conversion rates by integration usage](https://app.periscopedata.com/app/gitlab/756008/Mike-K-Scratchpad?widget=10229400&udv=0) | Dashboard | See the correlation between integration usage and conversion from free to paid | ongoing |
| [Stage Adoption Patterns: SCM <> Code Review <> CI](https://docs.google.com/presentation/d/1BcRhn8kJZTw8QcWSQLAk9mv72lJfk4d2jteGWCBYfo4/edit#slide=id.g29a70c6c35_0_68) | Analysis | How do organizations go from using SCM to Verify, and is Code Review used between or after these stages? | FY22 Q1 |
| [Secure Free-to-Paid Funnel and Feature Adoption Analysis](https://docs.google.com/presentation/d/1bbvfsNzKoZw4kCYB9coexiXzPiLZ5E3XPe6hOsbZlag/edit#slide=id.g29a70c6c35_0_68) | Analysis | Which Secure features are the most adopted during trials and which features have the highest correlation with Ultimate plan paid conversion? | FY22 Q2 |
| [Paid SpO Trends among Retained and Churned Namespaces](https://docs.google.com/presentation/d/1RR5qwaE2E2OUtfSgU53GMs8FHjexNx2CFJcUbtiNS-0/edit#slide=id.g29a70c6c35_0_68) | Analysis | What stage adoption trends are present when looking at churning namespaces vs.retained namespaces? | FY22 |








## Growth Experiments Knowledge Base

Here is a list of our currently running and concluded experiments.

Growth Product Managers reguarily update this knowledge base with upcoming, live and concluded experiments. 
Please lable product area for upcoming and live experiments for planning purpose. 

### Growth Experiments Knowledge Base - Upcoming Experiments

| Experiment Name | Group | Estimated Go-live Timeframe |[Product Area](https://gitlab.com/gitlab-org/growth/product/-/issues/1699)|
| ------ | ------ |------ |------ |
| [Create empty state for the security MR widget to drive secure adoption](https://gitlab.com/gitlab-org/gitlab/-/issues/333179) | Adoption | scheduled for 14.5 |MR page|
| [Improve the information presented after creating a new project](https://gitlab.com/gitlab-org/gitlab/-/issues/285579)| Adoption | scheduled for 14.6 | Project page | 
| [Drive Create Usage on Empty Projects via the MR Page Empty State](https://gitlab.com/groups/gitlab-org/-/epics/5025) | Adoption | scheduled for 14.6 |MR empty state page |
| [Improve the layout and appearance of the Registration Flow](https://gitlab.com/groups/gitlab-org/-/epics/5722) | Adoption | scheduled for 14.7 |Registration flow |  
| [Trial form: display known information](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/65493) | Conversion | Scheduled for 14.3 |TBD|
| [Add reassurance (social validation) to the SaaS trial signup page](https://gitlab.com/gitlab-org/gitlab/-/issues/281027) | Conversion | scheduled for 14.3 |TBD|
| [Follow Up Experiment- iterate on known fields within trial signup form](https://gitlab.com/gitlab-org/gitlab/-/issues/327039) | Conversion | scheduled for 14.3 |TBD|
| [Remind user of trial status when 14 & 3 days remain](https://gitlab.com/groups/gitlab-org/-/epics/3538) | Conversion | scheduled for 14.3 |TBD|
| [For single user namespaces add invite to left nav](https://gitlab.com/gitlab-org/gitlab/-/issues/224692) | Conversion | Q3 |TBD|
| [Move Billing to top-level navigation item](https://gitlab.com/gitlab-org/gitlab/-/issues/338540)| Conversion | 14.6 | Navigation | 
| [Add high-conversion trial feature tasks to Learn GitLab](https://gitlab.com/groups/gitlab-org/-/epics/7113) | Conversion | 14.7 | Continuous Onboarding | 
| [Promote pricing tier on /billing page] (https://gitlab.com/groups/gitlab-org/-/epics/7173) | Conversion | Q4 | SaaS Purchase flow | 
| [With no additional users add "invite" to nav for all free namespaces](https://gitlab.com/gitlab-org/gitlab/-/issues/224692) | Expansion | Q4 | Navigation | 

### Growth Experiments Knowledge Base - Active Experiments

| Experiment Name | Group | Go-live Date |Estimated Conclusion Date| Product Area|
| ------ | ------ |------ |------ |------ |
| [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/groups/gitlab-org/-/epics/6638)| Expansion | `2021-11-22` | Active | Registration flow |
| [Highlight paid feature during active trial](https://gitlab.com/gitlab-org/gitlab/-/issues/273626)  | Conversion | `2021-05-17` |TBD|TBD|
| [Add option to enable SAST at project creation to the new project flow SAST](https://gitlab.com/gitlab-org/gitlab/-/issues/333196) | Adoption | `2021-10-26` | `2021-11-30` |  Project creation |
| [Change invite email to be from inviter](https://gitlab.com/groups/gitlab-org/-/epics/5740) | Expansion | Active | Invitation acceptance flow |


### Growth Experiments Knowledge Base - Concluded Experiments

| Experiment Name | Group | Experiment Result | Go-live and Conclusion Date |
| ------ | ------ |------ |------ |
|[Add user invitation track to day zero for "company" namespaces](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/444/)| Activation | [Validated](https://gitlab.com/gitlab-data/product-analytics/-/issues/173) |  |
| [Template zero state page VS Simple template](https://gitlab.com/groups/gitlab-org/growth/-/epics/61) | Activation | [Invalidated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/545) |  |
| [Introduce users to verify via code quality template walkthrough](https://gitlab.com/groups/gitlab-org/growth/-/epics/68) | Activation | [Validated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/544) | |
|[In Product Email Campaigns](https://gitlab.com/groups/gitlab-org/growth/-/epics/62) | Activation | [Validated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/543) | |
| [Change invite email to be from inviter](https://gitlab.com/groups/gitlab-org/-/epics/5740) | Expansion | [Invalidated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/429#note_740336304) 2021-11-01 - 2021-11-23 |
| [Member Area of Focus](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/406) | Expansion | [Validated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/406#note_686459031) | `2021-08-18` - `2021-09-27`|
| [SaaS continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817) | Conversion | [Inconclusive](https://gitlab.com/gitlab-org/gitlab/-/issues/281024#note_646750697) | `2021-05-15` - `2021-08-10` |
| [SaaS trial onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4803) | Conversion | [Validated](https://gitlab.com/gitlab-org/gitlab/-/issues/276703#note_636521480) | `2021-02-10` - `2021-07-21` |
| [Invited user email - permission info vs activity](https://gitlab.com/gitlab-org/gitlab/-/issues/330353) | Expansion | [Inconclusive](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/411#note_651411575) | `2021-06-28` - `2021-08-16`  |
| [increase invite acceptance by removing exit CTAs](https://gitlab.com/gitlab-org/gitlab/-/issues/327239) | Expansion | [Invalidated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/379#note_595628831) | `2021-05-17` - `2021-06-08` |
| [SaaS trial active trial status countdown](https://gitlab.com/groups/gitlab-org/-/epics/3538) | Conversion |  ||
| [Optional trial during free SaaS signup](https://gitlab.com/groups/gitlab-org/-/epics/4414) | Conversion | [Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/251231#note_557303110) | |
|[Empty versatile template for CI](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/292) | Activation | [Concluded](https://gitlab.com/groups/gitlab-org/growth/-/epics/59#test-1-all-namespaces) | |
|[Empty versatile template for CI for <90 days>](https://gitlab.com/groups/gitlab-org/growth/-/epics/59) | Activation | [Concluded](https://gitlab.com/groups/gitlab-org/growth/-/epics/59#test-2-namespaces-90-days-old) | |
|[Update .com paid sign up flow](https://gitlab.com/gitlab-org/growth/product/issues/87)|Activation|Concluded| |
|[Pricing page tier presentation test](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/73)|Activation|Concluded| |
|[Clarify Auto renew toggle vs. Cancel](https://gitlab.com/gitlab-org/growth/product/-/issues/43) |Adoption |[Concluded](https://gitlab.com/gitlab-org/growth/product/issues/162)| |
| [Invited user email iteration 2](https://gitlab.com/gitlab-org/gitlab/-/issues/296966) | Expansion | [Validated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/325#note_536737613) | `2021-03-02` - `2021-03-24`  |
| [Add optional ‘invite colleagues’ to the registration flow (initial required onboarding steps)](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/351) | Expansion | [Validated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/351#note_575767034) | `2021-03-08 - 2021-05-20` |
| [Add invite option to comment area to drive more invite](https://gitlab.com/gitlab-org/gitlab/-/issues/296640) | Expansion | [Invalidated](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/300#note_620789810) | `2021-03-22 - `20201-07-07 |
|[MVC of What's New](https://gitlab.com/gitlab-org/growth/product/-/issues/1487) | Adoption | [Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/255349)| |
|[Make Upload Options Accessible from Project Details Page](https://gitlab.com/groups/gitlab-org/-/epics/4742) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4742#results-lessons-learned-next-steps) | |
|[Add CTA for Project Integrations to Drive Deeper Adoption](https://gitlab.com/groups/gitlab-org/-/epics/4772) | Adoption |[Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4772#results)| |
|[Display security navigation to upsell new .com signups](https://gitlab.com/gitlab-org/gitlab/-/issues/34910)|Conversion|Concluded| |
|[New user onboarding flow](https://gitlab.com/groups/gitlab-org/-/epics/3526)|Conversion|Concluded| |
| [Required group trials](https://gitlab.com/groups/gitlab-org/-/epics/4500)  | Conversion | Concluded | |
|[.com Trial flow improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/119015)|Conversion|[Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/215881)| |
|[MR with no pipeline Prompt](https://gitlab.com/gitlab-org/growth/product/issues/176)|Expansion|[Concluded](https://gitlab.com/gitlab-org/growth/product/-/issues/1588)| |
|[Buy CI minutes option on GitLab.com top right dropdown](https://gitlab.com/groups/gitlab-org/growth/-/epics/28)|Expansion|Concluded| |
|[Remove known fields from trial sign up form](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/281#note_540419469) | Conversion | Concluded | |
|[Add Ease Score and Interview Recruitment Track to Onboarding Email Series](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/314) | Activation | Concluded |  |
|[Add "New Repo" CTA](https://gitlab.com/groups/gitlab-org/-/epics/4776) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4776#results-lessons-learned-next-steps) |  |
|[Check "Initialize repository with a README" by default](https://gitlab.com/groups/gitlab-org/-/epics/4962) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4962#results-lessons-learned-next-steps) | |
|[Collect JTBD info in Registration Flow](https://gitlab.com/groups/gitlab-org/-/epics/4821) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4821#results-lessons-learned-next-steps) | |
| [Default Namespaces that are Setup for Company to Trial Flow by Auto Checking the Toggle](https://gitlab.com/gitlab-org/gitlab/-/issues/334829) | Adoption | [Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/334829#results-lessons-learned-next-steps) | `2021-07-28` - `2021-11-10` |
| [Provide guidance and a template in the default README](https://gitlab.com/groups/gitlab-org/-/epics/5461) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/5461#results-lessons-learned-next-steps) |`2021-08-06` - `2021-11-19`| 
| [Split Registration Flow based on if the user is joining an existing team or setting up a new one](https://gitlab.com/gitlab-org/gitlab/-/issues/337978) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/6630#results-lessons-learned-next-steps) | `2021-11-03` - `2022-01-20` | 
| [Simplify Group Creation in Registration Flow](https://gitlab.com/groups/gitlab-org/-/epics/4825) | Adoption | [Concluded](https://gitlab.com/groups/gitlab-org/-/epics/4825#results-lessons-learned-next-steps) |`2021-11-30` - `2022-01-20`| 
 
We rollout experiments using feature flags. These are tracked through [experiment rollout](https://gitlab.com/groups/gitlab-org/-/boards/1352542?label_name[]=experiment-rollout) issues.

## Growth Coffee & Learn Sessions

Growth requires constant learning, and many compaines face similar challenges or go through similar journeys. At GitLab, we believe hearing from growth experts and practioners with diverse background is the best way to learn, and we want to share that with the community as much as possible. Therefore, we started a growth Coffee & Learn series since 2020. Below is a list of exerperts we invited to share their wisdom with us. We appreciate their time and insights, note that some videos/notes are viewable by internal team only. 

|Expert |Background |Topic | Video |Notes|
| ---------     | ------ |----------------- |------ |------ |
|[Chris More](https://www.linkedin.com/in/chrismore/) |VP Growth @ Fusebit & Former Head of Growt@ Mozilla  | AMA on Open source & growth |NA|[AMA Notes](https://docs.google.com/document/d/1qhLH8D8vwRGe_PTOhhJwjuWsqPLF4pF1uuS5bK_t1E4/edit?usp=sharing) |
|[Holly Chen](https://www.linkedin.com/in/holly/)|Growth advisor, former head of global marketing @ Slack  |Growth Loop & Journey of Slack |[Video](https://www.youtube.com/watch?v=CFwKMUJv1Xo&list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf) |[Summary](https://docs.google.com/document/d/12LK9MqPl-PbabJkGwhxS2Uv-gjlOHNZOAX5DUqgvXq8/edit) |
|[Linsha Chen](https://www.linkedin.com/in/linshachen/)|Head of Growth Data Science, Airbnb|How Airbnb does experimentation |[Video](https://youtu.be/RaS62OLHDVY) |[AMA Notes](https://docs.google.com/document/d/1-EV8PfMPqAEj344sqY2OkFbeg4JC204jFmK_7it9N44/edit) |
|[Jike Chong](https://www.linkedin.com/in/jikechong/)|Author, How to lead in Data Science, former data science exectutive@ Linkedin, Acorns|How to Lead in Data Science |[Video](https://youtu.be/4kI8ICt8ylk) |[AMA Notes](https://docs.google.com/document/d/1t7PjAGjEqMQn0bFPUYEKJrwH6sb85VxKi5rd0kuzKUA/edit?usp=sharing) |
|[Anuj Adhiya](https://www.linkedin.com/in/anujadhiya/)|Author, Growthhacking for Dummies, VP Growth|High Performing Growth Teams|[Video](https://youtu.be/a3VIrCSq_k8) |[AMA Notes](https://docs.google.com/document/d/1cu_NZ_kAv72uT6ffHbUSm71h1QTQDYpoFT40lAETerM/edit?usp=sharing) |
|[Robert Neal](https://www.linkedin.com/in/robert-neal-1bb9ba144/)|Director of experimentation @ LaunchDarkly|Experimentation system AMA|[Video](https://youtu.be/eV7hjA74_gQ) |[AMA Notes](https://docs.google.com/document/d/1kjWvFeVmpvsRIPuEtfTBNpiINUMlE10MLB4kkQIQ9yE/edit?usp=sharing) |
|[Carl Gold](https://www.linkedin.com/in/carlgold/)|Former Chief Data Scientist, Zuora| Fighting Churn with data|[Video](https://youtu.be/Ntbrp0Crbtk) |[AMA Notes](https://docs.google.com/document/d/1k2oMsxO6bI6dnFIpvsz8YJp5cQEcrnsFj3pGpIEJxEA/edit?usp=sharing) |













## Growth Section Areas of Focus

### 1. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [About.GitLab.com](https://about.gitlab.com/)

### 2. Growth Group Directions

* [Product Analysis Direction](https://about.gitlab.com/direction/product-analysis/)

### 3. What's next: Growth Group Issue Boards


* [Activation board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion)
* [Adoption Board](https://gitlab.com/groups/gitlab-org/-/boards/2112028?&label_name[]=group%3A%3Aadoption)
* [Product Analysis Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&state=opened&label_name[]=product%20analysis)

## Growth Definitions

It can be confusing to know what we're all talking about when we use different names for the same things. This is where we can clarify and discuss common terms to make communication easier.

- *Sign-up Experience:* Registering to be a user of GitLab. It generally starts on `about.gitlab.com` and ends when the user is registered and can log in to the application. We have free, paid or trial options.
- *Registration Flow:* The "happy path" flow a new user goes through when registering for GitLab and landing in the app for the first time. This is a more confined user flow, as we narrow down options so they can experience GitLab without being overwhelmed by all the other features. It starts at `about.gitlab.com` and ideally ends with them using Create features.
- *Trial Experience:* The experience of selecting and starting a trial, as well as testing/trialing features in GitLab.com or a self-managed instance during the trial. This could be done by net new users of GitLab, or existing users of GitLab who are interesting in exploring paid features.
- *User Onboarding:* A general term that refers to experiences we create to support users during their first 90 days, or any time they engage with a new stage.
  - *Continuous onboarding:* Specifically our onboarding experience that allows users to choose when and if they want to continue onboarding tasks. It starts with simple tasks and builds to more complex ones.
  - *Onboarding Emails:* Refers to emails that are sent to Saas and self-managed users intended to drive stage adoption. 
- *Dashboard:* The "homepage" that is shown post-login to users.
- *SaaS Purchase Flow* - We consider the SaaS purchase flow (or "funnel") to begin with the /billings page and continue into the payment sequence that a user can access by selecting the "Upgrade" CTA from the /billings page with the last step of the flow being the successful first-time purchase of a GitLab paid tier.

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)

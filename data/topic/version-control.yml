title: What is version control?
header_body: >-
  Version control enables rapid collaboration and iteration on new features,
  helping teams deliver business value and meet customer demand.


  [Learn how to streamline development→](https://learn.gitlab.com/scm?utm_content=topicpage&utm_campaign=vccusecase){:data-ga-name="Learn how to streamline development"}{:data-ga-location="header"}
description: Version control software is used to track revisions, solve
  integration conflicts in code, and manage different artifacts involved in
  software projects.
canonical_path: /topics/version-control/
file_name: version-control
twitter_image: /images/opengraph/gitlab-blog-cover.png
related_content:
  - title: Version control best practices
    url: /topics/version-control/version-control-best-practices/
  - title: What is Git version control?
    url: /topics/version-control/what-is-git-version-control/
  - title: What is Git workflow?
    url: /topics/version-control/what-is-git-workflow/
  - title: What is InnerSource?
    url: /topics/version-control/what-is-innersource/
  - title: How to implement version control
    url: /topics/version-control/how-implement-version-control/
  - title: What are software team collaboration best practices?
    url: /topics/version-control/software-team-collaboration/
  - title: The benefits of distributed version control systems
    url: /topics/version-control/benefits-distributed-version-control-system/
  - title: What is GitLab Flow?
    url: /topics/version-control/what-is-gitlab-flow/
  - title: What are GitLab Flow best practices?
    url: /topics/version-control/what-are-gitlab-flow-best-practices/
  - title: What is a centralized version control system?
    url: /topics/version-control/what-is-centralized-version-control-system/
  - title: What are the most effective features for code review tools?
    url: /topics/version-control/what-are-best-code-review-tools-features/
  - title: What is a code review?
    url: /topics/version-control/what-is-code-review/
  - url: /topics/version-control/what-is-gitlab-flow/
    title: GitLab workflow overview
cover_image: /images/topics/version-control.svg
body: >-
  ## What is version control?


  The purpose of version control is to allow software teams track changes to the code, while enhancing communication and collaboration between team members. Version control facilitates a continuous, simple way to develop software.


  > **Version control** enables teams to collaborate and streamline development to resolve conflicts and create a centralized location for code.


  [Source code](/stages-devops-lifecycle/source-code-management/){:data-ga-name="Source code"}{:data-ga-location="body"} acts as a single source of truth and a collection of a product’s knowledge, history, and solutions. Version control (or code revision control) serves as a safety net to protect the source code from irreparable harm, giving the development team the freedom to experiment without fear of causing damage or creating code conflicts. If developers code concurrently and create incompatible changes, version control identifies the problem areas so that team members can quickly revert changes to a previous version, compare changes, or identify who committed the problem code through the revision history. With version control systems, a software team can solve an issue before progressing further into a project. Through code reviews, software teams can analyze earlier versions to understand how a solution evolved.


  Depending on a team’s specific needs, a version control system can be local, centralized, or distributed. A local version control system stores files within a local system. Centralized version control stores changes in a single server. A distributed version control system involves cloning a Git repository.


  [Learn five ways to enhance team collaboration with version control best practices→](/resources/ebook-version-control-best-practices/){:data-ga-name="VC best practices"}{:data-ga-location="body"}


  ## Why use version control?


  Software is developed to solve a user problem. Increasingly, these solutions have many different forms (e.g. mobile, embedded, SaaS) and run a variety of environments, such as cloud, on-prem, or Edge. As organizations accelerate delivery of their software solutions through DevOps, controlling and managing different versions of application artifacts - from code to configuration and from design to deployment - becomes increasingly difficult. Velocity without robust version control and traceability is like driving a car without a seatbelt.


  Version control facilitates coordination, sharing, and collaboration across the entire software development team. Version control software enables teams to work in distributed and asynchronous environments, manage changes and versions of code and artifacts, and resolve merge conflicts and related anomalies.


  [Read how Git Partial Clone lets you fetch only the large files you need→](/blog/2020/03/13/partial-clone-for-massive-repositories/){:data-ga-name="Partial clone"}{:data-ga-location="body"}
benefits_title: Benefits of version control
benefits:
  - title: Quality
    description: Teams can review, comment, and improve each other’s code and assets.
    image: /images/icons/computer-test.svg
  - title: Acceleration
    description: Branch code, make changes, and merge commits faster.
    image: /images/icons/pillar-speed-alt.svg
  - title: Visibility
    description: Understand and spark team collaboration to foster greater release
      build and release patterns.
    image: /images/icons/first-look-influence.svg
benefits_2_title: Which is the best version control software?
benefits_2_description: There are several source code management options that
  can help your team streamline development. The three most well-known options
  are Git, SVN, and Mercurial.
benefits_2:
  - title: Git
    description: Git is the most popular option and has become synonymous with
      “source code management.” Git is an open source distributed system that is
      used for software projects of any size, making it a popular option for
      startups, enterprise, and everything in between.
    image: /images/icons/logo-git.svg
  - title: Subversion
    description: SVN is a widely adopted centralized version control system. This
      system keeps all of a project's files on a single codeline making it
      impossible to branch, so it’s easy to scale for large projects. It’s
      simple to learn and features folder security measures, so access to
      subfolders can be restricted.
    image: /images/icons/logo-svn.svg
  - title: Mercurial
    description: Mercurial is a distributed version control system that offers
      simple branching and merging capabilities. The system enables rapid
      scaling and collaborative development, with an intuitive interface. The
      flexible command line interface enables users to begin using the system
      immediately.
    image: /images/icons/logo-mercurial.svg
cta_banner:
  - title: How does version control streamline collaboration?
    body: >-
      Version control coordinates all changes in a software project, effectively
      tracking changes to source files, designs, and all digital assets required
      for a project and related metadata. Projects without version control and
      collaboration can easily devolve into a tangled mess of different versions
      of project files, hindering the ability of any software development team
      to deliver value. With a strong version control solution, software teams
      can quickly assemble all critical project files and foster actionable
      communication to improve code quality. Stakeholders from across a DevOps
      team can collaborate to build innovative solutions - from product managers
      and designers to developers and operations professionals.


      #### [Discover 15 best practices for large teams to innovate and collaborate using source code management→](https://page.gitlab.com/resources-ebook-scm-for-enterprise.html){:data-ga-name="SCM for enterprise"}{:data-ga-location="body"}
    cta: []
resources_title: Next steps in version control
resources_intro: >
  Ready to learn more about version control? Here are a few resources to help
  you get started on your journey.


  * [Learn how GitLab streamlines software development →](/solutions/benefits-of-using-version-control/){:data-ga-name="Benefits of version control"}{:data-ga-location="body"}
resources:
  - title: Read how version control and collaboration builds a strong DevOps
      foundation
    url: /solutions/benefits-of-using-version-control/
    type: Articles
  - url: /webcast/collaboration-without-boundaries/
    title: Learn how to collaborate without boundaries to unlock faster delivery
      with GitLab
    type: Webcast
  - type: Video
    url: https://page.gitlab.com/resources-demo-scm.html
    title: Discover how code review and source code management streamline
      collaboration
  - url: /customers/cook-county/
    title: Learn how Cook County assesses economic data with transparency and
      version control
    type: Case studies
  - url: /customers/worldline/
    title: Learn how Worldline uses GitLab to improve code reviews
    type: Case studies
  - type: Books
    url: /resources/ebook-git-branching-strategies/
    title: Discover a Git branching strategy to simplify software development
  - url: /resources/ebook-version-control-best-practices/
    title: Version control best practices eBook to accelerate delivery
    type: Books
  - title: Read how Remote uses GitLab to meet 100% of deadlines
    url: /customers/remote/
    type: Case studies
  - url: /resources/whitepaper-moving-to-git/
    title: Learn how to move to Git
    type: Whitepapers
  - url: /customers/dublin-city-university/
    title: Read how Dublin City University uses GitLab SCM and CI to achieve top
      results
    type: Case studies
  - title: Watch how GitLab SCM and code review spark velocity
    url: https://page.gitlab.com/resources-demo-scm.html
    type: Video
suggested_content:
  - url: /blog/2020/04/07/15-git-tips-improve-workflow/
  - url: /blog/2020/11/19/move-to-distributed-vcs/
  - url: /blog/2020/03/05/what-is-gitlab-flow/
schema_faq:
  - question: What is version control?
    answer: Version control is software used to track revisions, solve integration
      conflicts in code, and manage different artifacts involved in software
      projects (e.g. design, data, images). Version control also enables
      frictionless communication, change, and reproducibility between team
      members.
    cta:
      - url: https://about.gitlab.com/topics/version-control/#what-is-version-control
        text: Learn more about Version Control.
  - question: Why use version control?
    answer: Version control facilitates coordination, sharing, and collaboration
      across the entire software development team. Version control software
      enables teams to work in distributed and asynchronous environments, manage
      changes and versions of code and artifacts, and resolve merge conflicts
      and related anomalies.
    cta:
      - url: https://about.gitlab.com/topics/version-control/#why-use-version-control
        text: Read more about Why use version control?
  - question: Which is the best version control software?
    answer: There are several source code management options that can help your team
      streamline development. The three most well-known options are Git, SVN,
      and Mercurial.
    cta:
      - url: https://about.gitlab.com/topics/version-control/#best-version-control-software
        text: Learn more about the best version control software

title: Parimatch
file_name: parimatch
canonical_path: /customers/parimatch/
cover_image: /images/blogimages/pmbet_cover.jpg
cover_title: |
  How Parimatch scores big with GitLab
cover_description: |
  Learn how GitLab Premium helps Parimatch enhance visibility, collaboration, and integration management.
twitter_image: /images/blogimages/pmbet_cover.jpg
twitter_text: Learn how @mParimatchUK uses GitLab CI/CD to increase operational efficiency.
customer_logo: /images/case_study_logos/Logo-Parimatch-yellow-CMYK.png
customer_logo_css_class: brand-logo-tall
customer_industry: Gaming
customer_location: Cyprus
customer_solution: GitLab Premium
customer_employees: |
  2,000
customer_overview: |
  Parimatch, a gaming platform designed for football fans to bet on matches, adopted GitLab to eliminate toolchain maintenance
customer_challenge: |
  Parimatch needed a solution to improve integration maintenance, update a complex toolchain, and meet compliance standards. 
key_benefits: >-

  
    Increased operational efficiency  

  
    Decreased manual overload with GitOps 

  
    Enhanced collaboration

  
    Intuitive user interface

  
    JIRA integration with GitLab
customer_stats:
  - stat: 3x 
    label: Deployment reduction
  - stat: 57%    
    label: User growth increase in 1 year
  - stat: < tools
    label: 4 tools down to 1
customer_study_content:
  - title: the customer
    subtitle: A gaming platform dedicated to football fans
    content: >-
    
  
       Parimatch is a popular platform for football fans to bet on matches. With a rich 25-year history, Parimatch is known for providing leaderboard competitions, boosted odds, and cash back incentives to encourage fans to join in the fun of betting before and during matches. Created by football fans, Parimatch strives to provide fans with a unique and engaging experience.
     
  - title: the challenge
    subtitle:  Complex integrations management and insecure infrastructure 
    content: >-
    
  
        Like many scaling organizations, Parimatch struggled to increase operational efficiency while managing and maintaining multiple tools. Developers used a variety of tools according to their personal preferences, so the team lacked a cohesive, standard workflow. Team members lacked the ability to collaborate easily without having a singular solution in place.
    
  
        The team also spent a significant amount of time maintaining the complicated toolchain and combating context switching as they moved from one application to the next. The time spent on maintenance pulled developer focus away from feature development and slowed velocity. 
    
  
        In addition to workflow difficulties, Parimatch also experienced a unique regulation challenge. As an organization that handles financial information and operates in the gaming industry, Parimatch has to adhere to strict security and compliance requirements, and the previous infrastructure didn’t meet these stringent regulations.
    
  
        In the past, the development team was able to get by using legacy tools, like Jenkins and TeamCity, but an enhanced focus on modernizing tools, maintaining compliance, and simplifying the development workflow inspired the team to seek a new solution.
  - title: the solution
    subtitle:  A single application simplifies integrations and increases collaboration
    content: >-
        In assessing the team’s needs, Parimatch’s ideal solution included a single application that enabled multiple integrations, features that created a secure infrastructure, and an open source foundation. 
    
  
        When considering options, the team immediately dismissed GitHub based on cost and infrastructure incompatibility. They also decided Jenkins was too complex and clunky for their needs. Team members had been using GitLab Free for several years and expressed satisfaction with the features and experience, so PM Bet moved 458 developers to GitLab.
    
  
        GitLab easily solved the integration issue that had been frustrating the team for quite some time. “We chose GitLab because it makes it easy to maintain multiple integrations in one place. If teams prefer different tools, they can continue to use them seamlessly without struggling with complex workflows or context switching. Some developers prefer Jira, and GitLab provides a simple integration to satisfy those contributors,” said Anatolii Kovalenko, Senior DevOps Engineer. 
    
  
        GitLab’s features meet Parimatch’s commitment to [compliance and security](/solutions/dev-sec-ops/), and the team appreciates that they can manage their own instance. Developers have been happy with GitLab’s user-friendly UI. “GitLab is the best wrapper for Git, and the web interface is very intuitive. In comparison with Jenkins, GitLab has a much nicer UI,” Kovalenko said.
    
  
        Using GitLab, Parimatch increased operational efficiency and collaboration across the entire software development lifecycle. 

  - title: the results
    subtitle: Increased operational efficiency 
    content: >-
    
  
        Parimatch has experienced an increase in collaboration as teams across the software development lifecycle embraced GitLab. “Many teams use GitLab, including developers, IT, security, and DevOps engineers. Everyone can cooperate efficiently with each other to meet their needs. Every team has their own criteria on how to deliver value, and now we can answer questions in a single place,” Anatolii explained. Because many teams are now using GitLab, there is an increase in visibility and communication, which makes for a stronger application.
    
  
        Anatolii shared that GitLab has positively impacted the team’s ability to maintain stability and ship high-quality code. “If something goes wrong, GitLab offers a simple fix. For example, if we deployed the wrong ratio of our services to the server, we can see the history, read the commit message, and identify the author to roll back commits.”
    
  
        The move to GitLab also resulted in a new deployment strategy: GitOps. This framework enables the team to enhance collaboration on infrastructure changes, deliver rapidly, and reduce risk. Using GitLab, Parimatch is able to drive infrastructure automation and benefit from integrated [CI/CD pipelines](/stages-devops-lifecycle/continuous-integration/).
    
  
        Parimatch has increased collaboration, simplified integrations, and strengthened operational efficiency. “GitLab, for us, is one source of truth,” Anatolii shared. 


    
  
        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: We chose GitLab because it’s the industry standard, and a good open source project for any development process
    attribution: Anatolii Kovalenko
    attribution_title: Senior DevOps Engineer, Parimatch




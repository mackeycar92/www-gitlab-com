---
layout: handbook-page-toc
title: United States Leave of Absence Policies
description: "GitLab's United States Leave of Absence policies."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab's leave policies are intended to provide team members time away from work to care for themselves as they recover from a serious health condition, care for a family member recovering from a serious health condition, to bond with a newborn or a newly placed child for adoption.  

This page is designed to educate our team members about the different US-specific leave types available, how a team member can expect to be paid and if their job is protected.  These leave programs are coordinated and run concurrently with FMLA (Family Medical Leave Act), and other State leave programs team members are eligible for.  

Your Absence Managment team is here to help before, during, or after your leave.  If you have any questions related to your time away after reviewing this page, please contact leaves@gitlab.com to discuss or request a 1:1 with the Absence Management team. 

## Which leave is right for me?
Below are the GitLab leave policies available to US team members, as well as common examples of how each leave may be used.  This list may not be all inclusive, so if you have questions regarding which leave may be right for you please contact leaves@gitlab.com.  

| Leave Program | Description | Eligibility |
|:---:|:---:|:---:|
| Parental Leave | I am expecting a child<br>My partner is expecting a child<br>My family is adopting a child | 6 months in role<br><br>If less than 6 months,<br>eligible for 25 days |
| Family Medical Leave Act<br>(FMLA) | I am having surgery<br>My family member is seriously injured<br>I have recurring appointments for follow up care (chemotherapy, prenatal exams, etc)<br>I am bonding with my newborn or newly placed adopted child<br>I am caring for a family member seriously injured while on active duty | 1 year of service, AND<br>1250hrs worked in the <br>year immediately prior<br>to the start of leave |
| Military Leave (USERRA) | I am being deployed<br>I am a reservist reporting for extended training (5 days or more) | Provided written or <br>verbal notice prior to<br>leave (if able) |
| Unpaid Personal Leave | My situation doesn't really fit into any of the other leave program definitions | Fully performing in role<br><br>Manager approval |
| Bereavement Leave | A member of my immediate family passed away | N/A |

### How Much Time Do I Get Off?  Is my Job Protected?

| Leave Program | Total Time Off | Job Protection |
|:---:|:---:|:---:|
| [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) | 16 weeks (if 6 months in role or),<br>25 days if less than 6 months in role | Yes* |
| [Family Medical Leave Act<br>(FMLA)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#gitlab-inc-united-states-leave-policy) | Up to 12 weeks | Yes* |
| Military Leave (USERRA) | Cumulative 5 years | Yes, up to 5 years* |
| Unpaid Personal Leave | Up to 30 days<br>Requests in excess of 30 days require functional VP and group People Business Partner approval | No |
| Bereavement Leave | 5 days | No |

*Except in certain circumstances

### How Will My Pay Be Calculated?

| Leave Program | Pay Calculations |
|:---:|:---:|
| [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#payroll-processing-during-parental-leave) | 100% paid by GitLab (minus any State Disability and/or Paid Family Leave benefits) |
| [Family Medical Leave Act<br>(FMLA)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#payroll-processing-during-fmla) | First 25 days:  paid 100% by GitLab<br>After 25 days up to 12 weeks:  <br>    - 66.67% (STD) + 33.3% paid by GitLab per week<br>After 12 weeks:  <br>    - 66.67% (LTD) per month |
| [Military Leave (USERRA)](/handbook/people-policies/leave-of-absence/us/#compensation) | First 25 days:  paid 100% by Gitlab<br>After 25 days:  Unpaid |
| Unpaid Personal Leave | Unpaid |
| Bereavement Leave | 100% paid by Gitlab |

*If you live in a state where a disability or paid family leave benefit is available, you will be required to apply for this benefit and if you are approved GitLab will supplement any gaps. Please refer to GitLab's [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#payroll-processing-during-parental-leave) policy for more information. 

## What Happens Next?

Team members can use this [checklist](https://docs.google.com/document/d/1NCf6NcDkSn5TlBcPLnP0eyjxIWOQ5lVfRVR_cvdZ9zI/edit?usp=sharing) as a reference for what steps to complete before, during, and after you return from leave.  
    * This checklist is currently in its first iteration.  Please continue to check back for updates. 
    * For questions specific to your leave or pay, please contact leaves@gitlab.com 

## Applying for Leave

- [Apply For Parental Leave in the US](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#parental-leave)
- [Apply for Family Medical Leave Act (FMLA)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#how-to-apply)
- [Apply for Military Leave](/handbook/people-policies/leave-of-absence/us/#notification-of-supervisormanager)

## Additional Resources

### State-Specific Allowed Leaves
 
Each US state varies when it comes to types of leave employers are required to allow employees to take. GitLab's [Paid Time Off policy](/handbook/paid-time-off/) policy of "no ask, must tell" takes precendence but we still want our US team members to be aware of their specific types of leave in their state.
 
| Leave Type                              | State or City/Region |
|-----------------------------------------|-------|
| Bereavement Leave                       | IL, OR, WA (Tacoma)  |
| Blood/Bone Marrow/Organ Donation Leave  | AR, CA, CT, HI, IL, LA, ME, MD, MN, NE, NJ, NY, OR, SC, WI |
| Court Attendance/Witness Leave          | CA, CT, DC, FL, GA, HI, IL, IN, IA, KY, MD, MA, MN, MO, NV, ND, OH, OR, PA, RI, TX, UT, VT, VI, WI, WY |
| Crime Victims' Leave                    | AL, AK, AZ, AR, CA, CO, CT, DE, MD, MA, MI, MN, MS, MO, MT, NH, NY, OH, OR, PA, RI, SC, VT, VA, WY |
| Domestic/Sexual Violence Victims' Leave | AZ, AR, CA, CO, CT, DC, FL, HI, IL, IA, KS, ME, MD, MA, MI, MN, NV, NJ, NM, NY, NC, OR, PA, RI, TX, VT, VA, WA |
| Drinking and Driving Class Leave        | IA      |
| Drug/alcohol Rehabilitation Leave       | CA      |
| Election Officials' Leave               | AL, CA, DE, IL, KY, MN, NE, NC, OH, VA, WI  |
| Emancipation Day Leave                  | DC      |
| Emergency Evacuation Leave              | TX      |
| Legislative/Political Leave             | CT, IA, ME, MN, MT, NV, OK, OR, SD, TX, VT, WV      |
| Literacy Leave                          | CA      |
| Mobile Support Unit Leave               | IN      |
| Paid Family Leave                       | CA, CO, CT, DC, MA, NJ, NY, OR, RI, WA       |
| Pregnancy Disability Leave              | CA, CT, HI, IA, LA, MN, MT, NH, OR, WA      |
| Public Health Emergency (Quarantine/Isolation) Leave | AZ, CA (San Diego), CO, DE, IL (Cook Co), MA, MD (Montgomery Co), MI, MN, NJ, NY (Westchester Co & NYC), OR, PA (Pittsburgh), RI, SC, VT, WA  |
| School Activities/Visitation Leave      | CA, DC, IL, LA, MA, MN, NV, NJ, NC, OR, RI, TN, VT   |
| Student Leave                           | DC      |
| Volunteer Emergency Responder Leave     | AL, CA, CO, CT, DE, IL, IN, IA, KS, KY, LA, ME, MD, MA, MO, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OR, PA, RI, SC, TN, UT, WA, WV, WI |
| Voting Leave                            | AL, AK, AZ, AR, CA, CO, DC, GA, IL, IA, KS, KY, MD, MA, MN, MO, NE, NV, NM, NY, ND, OH, OK, SD, TN, TX, UT, VT, WV, WI, WY   |

## U.S Military Leave

GitLab, Inc. recognizes the obligation of those team members serving in any branch of the military or other uniformed services of the United States. Employment status within GitLab is protected by the Uniformed Services Employment and Reemployment Rights Act of 1994 (“USERRA”).

### Leave and Re-Employment

Team members who serve on active or reserve duty will be granted a leave of absence up to the maximum time required by law. GitLab is committed to preserving the job rights of team members absent on military leave in accordance with law.

### Compensation

Military leave runs concurrently with GitLab PTO. Team members on unpaid military leave will be paid at 100% of their salary for the first 25 days of leave. Exempt team members will not incur any reduction in pay for partial week absences for leave under this policy.

### Health Care Continuation

During a military leave of less than 31 days, a team member is entitled to continued group health plan coverage under the same conditions as if the employee had continued to work. For military leaves of more than 30 days, an employee may elect to continue their health coverage in accordance with USERRA and COBRA. For additional information on health care continuation contact the Absence Management team `leaves@gitlab.com`.

### Notification of Supervisor/Manager

Team members are expected to inform their supervisor/manager of their need for military leave as far in advance as possible. Team members must also record their leave in PTO by Roots and submit a copy of the military orders to leaves@gitlab.com.

### No Retaliation

Team members who request military leave will not be retaliated against or penalized in any manner. Any team member who believes they have been retaliated against in violation of this policy should notify the Absence Management team (leaves@gitlab.com) immediately.

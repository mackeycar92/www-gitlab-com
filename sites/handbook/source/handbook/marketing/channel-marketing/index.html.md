---
layout: handbook-page-toc
title: "Global Channel and Alliances Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## This page is intended for the following audiences

GitLab global and regional channel and alliances sales teams, global and local field sales teams, regional [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) teams, channel account managers, the global alliances team and product marketing team.

## Our Team
Global Channel and Alliances Marketing is a team of seasoned professionals supporting global channel and alliances sales objectives and providing support to the GitLab channel partner community and customers.  This small but mighty team is responsible for the development of channel and alliances marketing campaigns that leverage GitLab’s Go to Market Campaigns and value plays.  We work with partners to help them understand what the campaigns are, how to use them to help drive partner sourced opportunities, how to leverage upcoming GitLab webinars/webcasts to drive conversion and how to generate trials for GitLab that they can nurture with prospects.  Channel partners are ultimately responsible for the execution of these campaigns.  

The Global Channel marketing team provides support to partners by creating different campaign assets into leverageable go-to-market programs called Instant Campaigns that partners can easily pick up and run with their customer or prospect lists.  GitLab channel marketing is also responsible for the development, rollout and management of trial enablement programs by which partners can generate trials of GitLab directly from their website, and passing or providing trial leads generated from GitLab’s own website to be worked and converted by partners.  

Channel marketing also enables channel go to market efforts through MDF funding and management, by identifying upcoming field marketing events and activities that channel partners can participate in, and by identifying opportunities for channel partners to participate with Alliances partners and GitLab on joint go to market activities.

We partner with regional Channel Account Managers and Field Marketing regional teams to work closely with channel partners and GitLab Alliances for inclusion in their regional marketing plans.  

As a service bureau to a wide variety of teams, we have support functions that are both in and out of scope at this time.  We have ideated on a list of potential future service capabilities that, as this team is able to add resources and our business plan requires it, we will add to our list of service offerings.  To recommend that we add a service offering to this growing list of potential offerings, please create an issue.

## Current Service Offerings as of Q1 FY22

*   Administering Marketing Development Fund (MDF) to provide financial reimbursement for APPROVED partner marketing campaigns and events.  Requests must support GitLab GTM initiatives and adhere to the MDF Request Process detailed below. MDF reimbursement will be at 50% of the total campaign request.  There are three approval levels and each level has 2 business days to review and approve/decline the MDF proposal. 
*   Turnkey, integrated demand generation campaigns-in-a-box global and where appropriate, regional
    *   Support and funding for translation and localization of campaign assets from English to another language for [priority countries](https://docs.google.com/spreadsheets/d/1eRrtRPdNSQjtvDrEvPJ_klfqKAatnLIzDjvShXhnSr8/edit?usp=sharing) will be considered upon request For additional information on localization, please visit the [Localization](https://about.gitlab.com/handbook/marketing/localization/#priority-countries) Handbook page.
*   Global [external virtual events, workshops](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/), and third party events and webcasts  leveraging Alliance partner “better together” messaging and joint GTM solution customer value propositions.
*   In partnership with [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/): 
    *   Develop marketing campaign that targets partner Solution Architects to become advocates of GitLab by delivering GitLab solution overviews, demos and proof of concepts to uncover and convert sales opportunities
    *   Enable channel partner marketers to participate in [FY22 global GTM](https://about.gitlab.com/handbook/marketing/plan-fy22/) motions in partnership with the [GTM team](https://about.gitlab.com/handbook/marketing/plan-fy22/#core-teams).  
        *   Build and deliver a Bill of Materials of channel-friendly and consumable versions of GitLab campaigns and assets 
*   Provide review and approval for local/regional [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) plans with partners
*   Provide review and approval of Channel Account Manager (CAM) marketing plans.
*   Build a repeatable, predictable, sustainable and desirable model enabling partners to receive, accept, nurture and close GitLab leads leveraging the GitLab Partner Portal and Vartopia Deal Registration system.  (future link to documentation of the process)
*   Enable channel partners to add “[GitLab free trial](https://about.gitlab.com/free-trial/self-managed/)” functionality to their websites with marketing tools and lead passing to allow partners to effectively generate and nurture their own leads.  This will serve as partner primary CTA within demand generation efforts. 
*   For Select Partners, participate in partner QBRs and provide input and ideation on successful go to market campaigns and initiatives to help partners drive more business with GitLab. 
*   On a monthly basis, provide marketing content and input to the Global Channel Operations team for use in the global channel communication/newsletter 
*   On a quarterly basis, host global marketing webcasts to the partner community.
*   Support GitLab [Analyst Relations](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/) requests for channel program input on MQs, WAVEs, market guides, as needed and with third party channel analysts.  
    *   Support requests for inclusion in third party publications
*   Support the web team in maintaining GitLab’s marketing website [partner pages](https://about.gitlab.com/partners/)
*   Working with Sales Ops and Marketing Ops improve attribution and reporting of [partner sourced leads (CQLs), and partner sourced opportunities](/handbook/sales/field-operations/channel-operations/). 

## Quarterly Planning: Field and Channel

As GitLab evolves, the growth of the channel becomes a critical component to our overall strategy of growth and new logo capture.  With established teams in place, it's time to take our engagement up a level and as such, review how we partner together for proactive, and efficient engagement that results in high quality demand generation plans and delivering actionable leads for our sales teams.
In order for our collective plans to work, it's first important to verbalize the roles, responsibilities and focus areas for each functional team.

13 weeks prior to the start of the following quarter, planning for that quarter will commence.  FMM's and CAM's are expected to partner together and setup regular, consistent planning calls, preferrably bi-weekly. The focus of each cadence call will change as the plan formulates.  Specifically: 
*   ### Weeks 1-2 (11-13 weeks prior to the future quarter start):
Develop partner campaign objectives and set goals (specifically NFO SAO goals).  Set up recurring biweekly cadence with the individual partner, CAM and FMM to keep planning and campaign action items on track.
*   ### Weeks 3 and 4 (9-11 weeks prior to future quarter start): 
Based on campaign goals, identify the regions where pipeline needs are the greatest.  Architect campaigns to support that/those regions.  What are the campaign elements/tactics needed to create interest?
*   ### Week 5 and 6 (7-9 weeks prior to future quarter start): 
Agree to campaign tactics and persona targets, develop budget request and expected outcomes in the form of MQL's and SAO's.
*   ### Week 7 and 8 (5-7 weeks prior to future quarter start): 
Formalize/finalize the tactics, define action items and workback plan.  Identify any speaker needs.  Final agreement on MQL and SAO targets.  Complete FY23 Partner Planning Template and submit back to FMM and CAM.
*   ### Week 9 (4 weeks prior to future quarter start): 
FMM and CAM submit holistic budget request to Channel Marketing for MDF approval.

## Team Roles and Responsibilities
*   ## Channel Account Managers 
Own the strategy, relationship and ongoing, consistent engagement with the Select partners in their territory.  The channel account manager is responsible for the holistic success and health of the partner relationship. 

*   ## GEO Channel Director
 Sets the strategy for the region, manages the Channel Account Managers and approves the criteria for Select and Open partner selection in the region.
*   ## Field Marketing Managers
In partnership with field sales, drive MQL's to convert to SAO's.  The field marketing managers are responsible for articulating which areas of the business require the most attention and where we need channel partner engagement to drive demand generation plans.
*   ## GEO Field Marketing Director
Develops the strategy and conversion targets for the team in partnership with field sales leadership.
*   ## Global Channel Marketing Director
Develops scalable demand generation campaigns and tactics for partners to generate Partner Sourced opportunities.

Once a plan for the quarter has been established it is imperative that the channel account manager and the field marketing manager stay in close, consistent alignment about plan execution.  The strategy is driven by the channel account manager, while the field marketing manager is responsible for executing on that strategy in partnership with various support teams, including but not limited to the channel account managers.

[MDF](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds) 
CAN and SHOULD be leveraged as much as possible when the partner is involved in an activity.  Further information about how to initiate and MDF request can be found here.

## Tracking Success:
GitLab Marketing receives 'credit' for opportunities that are partner sourced so long as the oportunity has a BATP (Bizable Touchpoint) from the campaign.
Check out the [field marketing handbook page](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing-initiated-and-funded-campaigns)
that details the FMM process where CAMs can read more about how to complete [the lead gen issue.](https://gitlab.com/gitlab-com/marketing/field-marketing/-.issues/new?issuable_template=Channel_LeadGen_Req)

## Requesting MDF funds

- Approved Select Partners will submit [MDF proposals through the partner portal](https://docs.google.com/forms/d/1DTpKzLHiQmai7clipbHCv0pHX9Qz9EymnMHGPMMz3To/viewform?edit_requested=true), and GitLab approvers will be notified of the request via email generated from the Google form.
    + The Channel Marketing DRI will create a confidential new issue [`new_channel_mdf_request`](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#new_channel_mdf_proposal) for the review and approval process for the MDF proposal - This kicks off the official process
       +  All documentation, analysis, thoughts and approvals will be captured within that issue
       +  Each issue will be associated to the appropriate `Quarterly MDF Epic`
    +  The MDF proposal will go through 3 levels of approvals 
       + Level 1 approval - Channel Account Manager (CAM)
       + Level 2 approval - Channel Marketing
       + Level 3 approval - Field Marketing
          + For efficiency purposes, Channel Marketing will assign the Level 3 approver for FMM to the appropriate FMM Manager for the region and PubSec
          + The FMM Manager will then allocate to the appropriate FMM manager in the region of the MDF request. (see issue template for more details) 
    +  Once the MDF request has been either approved or declined the partner will be notified: 
          + If declined, the Channel Marketing DRI will reach out to the partner and let them know. We will copy the CAM on the notificiation
          + If approved, the FMM will be the DRI for reaching out to the partner contact in the proposal to review dates of execution and provide any other guidance
          + If approved, channel marketing manager will [open an epic](/handbook/marketing/channel-marketing/channel-marketing-epics/) and correlating sub-issues defined therein. 

### Demand Generation activities eligible for MDF
-  **Customer Case Study** - Development of a print or digital case study that details an end-user customer’s positive experience with GitLab. Must highlight how a GitLab product or service solved a customer’s business problem, benefit received by customer, and customer’s endorsement of GitLab.
-  **Telesales/Appointment Setting** - Outbound lead generation telemarketing campaigns that target end-user prospects /customers that promote GitLab or GitLab Alliance joint products or solutions.
-  **Digital & Print Marketing** - Creation of broad reach promotional and brand marketing content for GitLab products or solutions in digital or print targeted at end-user customers or prospects. Must contain a customer call to action.
-  **Paid Search Events/Webinars** (includes workshops, roundtables) - End-user customer or partner event executed by a Partner to generate net new business and / or build pipeline. Must be focused on GitLab and / or joint solutions with Alliance Partners.
 - **Tradeshows/Conference Sponsorships** - Partner on-site sponsorship of a Third-party event organized by an industry set, or other GitLab Partner, where a sponsorship contract / package is purchased by Partner and GitLab products, solutions, & brand are promoted.


These activities are captured as `Campaign Type Details` on the campaign level in Salesforce.com for `Partner - MDF` [Campaign Type](/handbook/marketing/marketing-operations/campaigns-and-programs/#campaign-type--progression-status) only.

### Submitting Proof of Performance (POP) for MDF 

- Once the MDF campaign is completed, the partner has 30 days to submit their [Proof of Performance (POP)](https://docs.google.com/forms/d/e/1FAIpQLSeb_r6c7xo4KdxXWbwelmAJiDyVAF5UxMxeppypH2C0oQ8vhQ/viewform) for reimbursement.
- Once a POP is completed, the Channel Marketing Manager will create an issue [`channel_mdf_pop`](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#channel_mdf_pop) and complete Step 1 
- Once Step 1 is completed, Channel Marketing will notify Finance for their review and reimbursement payment to the partner

## Requesting Swag for Channel events and awards
CAMs can now order swag for approved co-marketing events or other approved awards through Channel Marketing’s partner swag portal.  Please allow 10-15 business days for the approval and processing of your request plus additional shipping time. 

**To view the portal and see types of Swag available:**
1. Go to the portal: https://www2.nationsprint.com/clients/gitlab/
1. Create your own login using your gitlab email address
1. Once logged in, you can view Swag items currently available

If you are ordering Swag for one person (vs an entire event), you can place your own order using the portal as long as you have the recipient’s shipping address.  Make sure you have received approval from your manager to place the order.
Postcard notes are available to customize your shipment.  Just select the postcard from the ordering portal and add your own customized text to be printed on the notecard.

**To request Swag for a virtual event:**
1. Send an email to partner-marketing@gitlab.com.  Include the following details in your request: event DRI, partner name, name, date and type of event, expected number of attendees that would be eligible for swag, desired dollar amount of swag items and links to MDF request, event Issue, and any other pertinent information.  If you’d like a custom post card note included in your order, please provide the text so we can create the card as part of your order.
1. Once your request is approved, we will create a unique swag bundle with unique ordering url and provide you with the url you can share with the attendees post event.  This way each attendee can enter their own shipping information and process their own order request directly into the portal.

**To request Swag for an in-person event:**
1. Send an email to partner-marketing@gitlab.com.  Include the following details in your request: event DRI, partner name, name, date and type of event, event shipping address including attn info any required shipping info, number and type of items being requested, first and last date items can arrive and whether you need a return label for non-used items.  Please allow 15 business days for the approval and processing of your request.  

## Partner-Initiated GitLab Free Trial Lead Gen Program

Partners now have the ability to place a FREE trial button on their website as a critical call to action to their demand generation plans. Free trials allow users to have 30 day access to the full set of features in the Self-Managed version of GitLab. Once a visitor requests a free trial from a GitLab partner, the record is sent to GitLab for the license key to be generated and assigned to a partner in SFDC. In addition, the lead is then routed automatically via Vartopia to the partner for tracking and follow up. This is the same process that is followed whether the record is considered a lead or a contact within GitLab's instance of SFDC.

Partners can access and manage their GitLab leads in the same interface as their [deal registration](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview). When leads are shared with partners, partners will now see an additional tab in the Vartopia GitLab Deal Registration portal called Prospects. Partners can also easily convert their prospects to deal registrations and submit them to GitLab for approval. Please refer to the following assets to walk you through how to access these leads, step by step: 

*   [Reference the User Guide](https://drive.google.com/file/d/1IYOPfvwIWgUIknF4JZdTM4BIO-lmm0U2/view?usp=sharing) 
*   [How-to video](https://youtu.be/e3qXq6TGbMY) 
*   [GitLab technical setup](/handbook/marketing/channel-marketing/partner-campaigns/)

## Future/Potential Service Offerings

*   Branding, awareness and promotion of GitLab channel partner badges
*   Partner marketing concierge services (dependent on funding/resources)
*   Joint demand generation campaigns with partners
*   Partner Sales Engineer/Solution Architect marketing program (to become advocates of GitLab)


## Beyond our Team’s Scope



*   Individual partner support, planning, ideation and execution including but not limited to joint demand generation campaigns.  Please work closely with your region’s [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) team and reference linked Handbook pages for self support and field support of these programs.
*   Custom campaigns: Channel Marketing does not currently have the resources to support individual partner campaigns and events.  Please encourage your partners to leverage one of our turnkey, and integrated campaigns-in-a-box.  For unique virtual and live events, the CAM should engage with their [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) counterpart to leverage field budget and marketing support for joint events.  The Field Marketing Manager will engage with their sales teams and support the CAM and partner with development, execution, and lead follow up for  the event.
*   Asset creation: Channel Marketing is unable to work directly with partners to customize their marketing assets. 
*   Joint demand generation planning and execution:** **The GitLab CAM is the primary point of contact to enable their partners run a successful campaign or event.  
*   Event speakers: The Channel Marketing team does not have the resources to help locate GitLab speakers for partner events.
*   Partner Blogs: The Channel Marketing team does not have the resources to craft unique content to support a partner blog or content request.
*   Press releases: The Channel Marketing team does not have the responsibility to edit and approve partner press releases.  Please create a [Channel or Tech Partner Announcement issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=channel-partner-announcement-request) for coordination with the Corporate Communications/PR team
*   Sponsored social media posts: partners looking to GitLab to promote partner activities would not route those requests through Channel Marketing.  Instead, CAM’s obtain the partner’s social media channels and [create an issue](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#open-a-new-issue-to-request-social-coverage) for the Social Media team.  This request should include the partner’s social media information, detailed description of what we are being asked to promote and target dates.  
*   Event attendance: The Channel Marketing team does not own a database of contacts by which to drive attendance to partner specific events nor do we possess the resources to support such requests.
*   Partner training:**<span style="text-decoration:underline;"> [Partner training](https://about.gitlab.com/handbook/resellers/training/)</span>** is managed and supported by the [Channel Partner Training, Certifications, and Enablement team](https://about.gitlab.com/handbook/resellers/training/).
*   Partner portal management: [Partner portal administration](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/#partner-portal-administration) is managed by GitLab Channel Program Operations.

## Service Level Agreement

When requesting support from our team, we commit to responding to your [issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new?issuable_template=channel_partner_request) within 2 business days (when request is easily understood) or a Zoom meeting invite to understand the request, scope, timeline, goals and expectations.  

## Meet the Team



*   **David Duncan**: (aka Dunk) VP of Demand Generation & Partner Marketing
*   **Coleen Greco**: (aka Greco) Director, Global Channel and Alliances Marketing
        **Channel Marketing Team**
         * **Karen Pourshadi**: Senior Channel Marketing Manager.  Karen will be focusing on partner content: enabling our partner marketing resources with major GitLab GTM initiatives and developing turnkey, integrated [Instant Marketing Campaigns](https://about.gitlab.com/handbook/marketing/channel-marketing/campaigns/#what-is-a-partner-instant-marketing-campaign) for GitLab channel partners. 
         * **Sara Valko**: DMR Channel Marketing Manager.  Sara will be focusing on our DMR channel partners including but not limited to: CDW, SHI and Insight.  She will be partnering with them to develop integrated demand generation campaigns that drive net new logos for GitLab. 
         * **Samara Souza**: Senior Partner Program Manager.  Samara will be focusing on creating new service offerings for our partners to leverage in their demand generation motions including but not limited to: Free trial syndication; case study development, MDF process, and much much more! 
        **Alliances Marketing Team**
         * **Lisa Rom**: Senior Alliances Marketing Manager.  Lisa will be focusing on supporting our alliances marketing initiatives
         * **Lindsey Lopez**:  Alliances Marketing Manager.  Lindsey will be focusing on supporting our alliances marketing initiatives 



## The best way to contact our team is through our Slack channels

#channel-marketing 

#alliances_partnermktg

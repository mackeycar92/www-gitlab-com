---
layout: handbook-page-toc
title: "Remote.com"
description: "Discover the benefits for Remote team members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Remote](https://www.remote.com/) and apply to team members who are contracted through Remote. If there are any questions, these should be directed to the Total Rewards team at GitLab who will then contact the appropriate individual at Remote.

## South Africa

### Healthcare Monthly Allowance

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members for their own Medical Coverage.
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.
* Proof of coverage must be shared or forwarded to total-rewards@ domain in order to receive the correct allowance amount. 
* The allowance will be up to R5,000 per month for team member and up to R8,500 per month for team member plus Dependants.
* The Total Rewards team will email the proof of coverage to Remote to have the reimbursement processed. 

### Discovery Life Provident Umbrella Fund (Provident fund match)
 
Under the Remote Technology scheme, the employer and member both pay **5% each to the Provident fund** as contributions. Applicable tax laws provide that any contribution the employer makes is treated as a contribution made by the member. The contributions will qualify for a tax deduction in each tax year of assessment. 

### Group Life including the Global Education Protector & Funeral Family Benefit

* Sum insured of the Group Life Cover will be 4 times of the annual salary.
* The primary protection offered by group life cover is as follows:
    * Provide for living expenses for surviving dependants
    * Extinguish debt
    * Protect the lifestyle of the surviving dependants
    * Fund for the education costs of the surviving children
* Upon the death of the member, spouse or child, a benefit payment equal to the amount of the Funeral Cover will be made. 
* The sum assured for this benefit is up to R30,000
* This benefit is 100% contributed and covered by Remote.    

### Disability - Income Continuation Benefit

* The Income Continuation Benefit is designed to provide team member with a payment equal to the income they received before they became disabled or severely ill.
* Disability refers to injury, illness or disease that has resulted in a member being unable to perform his or her own job based on objective medical criteria.
* Due to changes in the tax treatment of income protection benefits, the disability income benefit will be calculated on a flat **75% of member salary**, but subject to a maximum of the team members’s net of tax salary or specified rand maximum including any retirement fund waiver benefits.
* The 75% income continuation will come into effect from the **4th month** of the leave.
* This benefit is 100% contributed and covered by Remote.

### Disability - Severe Illness

* A severe illness is an illness that affects a person’s lifestyle in such a way that their ability to function normally is altered.
* Sum assured of this benefit is twice the annual salary of the team member (2 x Annual Salary).
* Discovery Life provides insurance to cover team member against the impact of a severe illness. The Severe Illness Benefit pays a lump sum if a team member is diagnosed with a covered physiological or anatomical severe illness. The claim payment is proportional to the severity of the illness, with severity levels that have been set to reflect the financial impact of the illness on their lifestyle.
* The lump sum benefit provides financial assistance to ensure that the team member can maintain their lifestyle after a life-changing event. This could mean having their homes modified to accommodate their injury or illness, or reinvesting the money to replace the monthly income they can no longer earn
* This benefit is 100% contributed and covered by the Remote.

For more details on benefits managed by Discovery: [Remote Technology Employee Benefits](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/bfd6b16936b7cfaccc8fe0992bd7271f/ZA_Remote_Technology.pdf)

### Parental Leave

To initiate your parental leave, submit the dates via PTO by Roots under the Parental Leave category. This will prompt the Absence Management team to process your leave. You can find out more information about our Parental Leave policy on the [general benefits page](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

If you are eligible to receive parental leave from GitLab, your payments will be made up of payments from UIF and the rest from GitLab. Once you send your parental leave notification in PTO by Roots, the Absence Management team will notify Remote. They will then get in touch with you to explain the UIF payment claim process. 


## Mexico

### Social Security

All workers employed in Mexico must be registered with and contribute to the following institutions that deal with different social security insurance benefits:
* Mexican Institute of Social Security (IMSS):
    * Provides medical services, child day care, accident and sickness compensation, pregnancy benefits and disability pensions
* National Workers’ Housing Fund Institute (INFONAVIT):
    * Provides subsidized housing to employees as well as loans, and the Retirement Savings Program (SAR). SAR provides employees with retirement benefits when they reach 65 years of age.

Employees in Mexico are covered by the Social Security Law, under IMSS who is responsible for administering social security insurance benefits and the collection of contributions.
* Both the employer and employee are required to contribute to social security, although the employer has the responsibility of withholding the employee’s contribution.
* These contributions fund retirement pensions, health and maternity insurance, occupational risk, day-care, disability and life insurance, and unemployment/old age insurance.

### Medical Benefits

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members.
* The allowance will be 5000 USD per annum. 
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.

### Life Insurance

* Life insurance cover provided via Remote (Details to be added by Total Rewards)

### Christmas Bonus (Aguinaldo)

* GitLab offers 30 working days pay (which includes total earnings + taxable allowances + commissions)
* Paid by December 20th (so the employee can use it for the holiday)
* Employees with less than one year of service will recieve a pro-rated christmas bonus. 

### Vacation Bonus (Prima)

The vacation premium is an additional cash benefit given to employees for use on their vacation. It is calculated as a minimum of 25% of daily salary multiplied by the number of days of vacation. Employees who have provided one year of service must be afforded a minimum of 6 paid vacation days in their first year of employment. Two working days will be added to that vacation time every following year through the fourth year. After five years, employers are required to add two days of vacation time every five years.
* Here is a chart on how vacations days increase:

| Year(s)  | Days |
|----------|------|
| 1        | 6    |
| 2        | 8    |
| 3        | 10   |
| 4        | 12   |
| 5 to 9   | 14   |
| 10 to 14 | 16   |
| 14 to 19 | 18   |

**Calculation:**
* To calculate the vacation bonus based on 25% and a salary of $50,000 USD per year:
* $50,000 USD divided by 365 days = $136.99 USD daily pay rate
* $136.99 USD daily pay rate multiplied by 25% = $34.25 USD
* $34.25 USD multiplied by 6 vacation days (first year) = $205.50 USD vacation bonus
* It can be paid right away when the vacation is taken, or as a lump sum upon completing one year of service (or employment ends). It is the employer’s choice which method to use.
* Unused leave must also be paid when employment ends (in other words, the employee does not ‘forfeit’ unused vacation time)

### Mexico Parental Leave

#### Statutory General Entitlement 

**Maternity Leave:** Team members can take 12 weeks of Maternity Leave (6 weeks before the child is born and 6 weeks after birth). 

**Paternity Leave:** Team members can take 5 days of Paternity Leave.

#### Maternity Leave Payment

* 12 weeks of the team member's Maternity Leave will be paid by the Mexican Social Security Institute (MSSI). 
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the MSSI payments to ensure the team member receives 100% pay for up to 16 weeks of leave. 

#### Paternity Leave Payment

* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), the team member will receive 100% paid Parental Leave from GitLab for up to 16 weeks.

#### Applying for Parental Leave in Mexico
To initiate your parental leave, submit your time off by selecting the `Parental Leave category` in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).


### Mexico Sick Leave

Team members unable to work because of a nonwork-related injury or illness and who have made payments into the social security system for the four weeks before the condition developed are eligible for paid sick leave through the Social Security Institute. The benefit, which is 60% of an employee’s regular wage, is paid from the fourth day of the illness for up to 52 weeks and maybe extended for another 52 weeks. 

## Hungary

### Social Security

The Hungarian Social Security Act has employer and team member contributions to cover statutory requirements such as pension, health insurance, and unemployment. 

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Hungary can access the public health system for free or at a lower cost through the Hungarian state healthcare system. This system is funded by the National Health Insurance Fund.

### Pension

GitLab does not plan to offer pension benefit at this time as the Hungarian pension system provides for a minimum pension, with a qualifying condition of minimum 20 years of service, of HUF 28,500 per month. If the average contribution base is less than the amount of the minimum pension, the pension will equal 100% of the average monthly wage.

### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from [Social insurance system](https://tcs.allamkincstar.gov.hu/) if they get ill, injured or have a disability.

### Hungary Parental Leave

#### Statutory General Entitlement

**Maternity Leave:** Team members can take up to 24 weeks of Maternity Leave. The leave must start 4 weeks prior to the scheduled due date.

**Paternity Leave:** Team members can take 5 days of Paternity Leave or 7 days in case of twins.

**Parental Leave:** Team members can take unpaid leave to care for their child until the child reaches the age of 3 or until the child reaches the age of 10 if the team member receives child care allowance. Team members are also entitled to extra vacation days based on the number of children they have: 

* two working days for one child; 
* four working days for two children; 
* a total of seven working days for more than two children under sixteen years of age.

#### Matenity Leave Payment

* The team member will receive Pregnancy and Confinement Benefit (CSED) at a rate of 70% of their salary for 24 weeks.
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the CSED payments to ensure the team member receives 100% pay for up to 16 weeks. 

#### Paternity Leave Payment

* The team member will receive payment from the Hungarian State Treasury for 5 days of their leave.
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the State payments to ensure the team member receives 100% pay for up to 16 weeks. 


## Austria

### Social Security

The Austrian Social Security Act General Social Insurance Act (Allgemeines Sozialversicherungsgesetz, ASVG) has employer and team member contributions to cover statutory requirements such as pension, health insurance, accident insurance and unemployment. 

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Austria can access the public Austrian health insurance system. This health insurance scheme covers all the team members and their family members.  

### Pension

GitLab does not plan to offer pension benefit at this time as Austria has their uniform pension system through The Act on the Harmonisation of Austrian Pension Systems. The amount of a pension is calculated on the basis of the duration of the pension insurance and amount of the contributions paid. In order to receive the pension a team member must have paid contributions for at least 180 months (15 years).

### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from Social insurance system if they get ill, injured or have a disability.

###  Bonus Payment

The salary will be paid paid 14 times a year. This includes 12 months salary and two bonuses. 

### Remote Technology - Austria Leave Policy

#### Statutory Maternity Leave

The statutory entitlement for maternity leave (Mutterschaftsurlaub) is 16 weeks. The leave must start **8 weeks prior to the scheduled delivery date**. For high-risk births, leave after the birth can be extended to 12 weeks. 

#### Statutory Parental Leave

Mothers and fathers are entitled to parental leave until the child reaches the age of 24 months (maximum), provided the parent in parental leave lives in the same household as the child. The minimum period of the parental leave is two months. The dismissal and termination protection ends four weeks after the end of the parental leave.

During the time of parental leave, and provided the conditions are satisfied, parents are entitled to childcare allowance(Kinderbetreuungsgeld) under the Child Care Payment Act from social security. 
